#ifndef HAVE_INTEGRATORS_H
#define HAVE_INTEGRATORS_H
double adapt24(double f(double), double a, double b, double acc, double eps, double *err, double f2, double f3, int n_rec);
double adapt(double f(double), double a, double b, double acc, double eps, double* err);
double adapt_clenshaw_curtis(double f(double), double a, double b, double acc, double eps, double* err);
double adapt_a_to_infinity(double f(double), double a, double acc, double eps, double* err);
#endif
