#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<assert.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"integrators.h"

double adapt24(double f(double), double a, double b, double acc, double eps, double *err, double f2, double f3, int n_rec){

	assert(n_rec < 1e6);
	double f1 = f(a+(b-a)/6.0);
	double f4 = f(a+5.0*(b-a)/6.0);
	double Q = (2*f1 + f2 + f3 + 2*f4)/6.0 * (b-a); /*trapezium rule*/
	double q = (f1 + f2 + f3 + f4)/4.*(b-a); /*rectangle rule*/
	double tol = acc + eps*fabs(Q);
	*err = fabs(Q-q);

	if(*err < tol){ /*check error*/
		return Q;
	}
	else{/*if error too large, recursively split integral into smaller parts*/
		double Q1 = adapt24(f,a,(a+b)/2.0,acc/sqrt(2.0),eps,err,f1,f2,n_rec+1);
		double Q2 = adapt24(f,(a+b)/2.0,b,acc/sqrt(2.0),eps,err,f3,f4,n_rec+1);
		return Q1 + Q2;
	}
}

double adapt(double f(double), double a, double b, double acc, double eps, double* err){
	/*adaptive integrator*/
	double f2 = f(a+2.0*(b-a)/6.0);
	double f3 = f(a+4.0*(b-a)/6.0);
	int n_rec = 0;
	return adapt24(f,a,b,acc,eps,err,f2,f3,n_rec);
}

double adapt_clenshaw_curtis(double f(double), double a, double b, double acc, double eps, double* err){
	double f_clenshaw_curtis(double theta){
		double f_cos_theta = f((a+b)/2.0+(a-b)/2.0*cos(theta));
		return f_cos_theta * sin(theta)*(b-a)/2.0;
	}
	double a_new = 0;
	double b_new = M_PI;
	return adapt(f_clenshaw_curtis,a_new,b_new,acc,eps,err);
}
double adapt_a_to_infinity(double f(double), double a, double acc, double eps, double* err){
	double f_eq60(double t){
		double f_t = f(a+(1.0-t)/t);
		return f_t * 1/t * 1/t;
	}
	double a_new = 0;
	double b_new = 1;
	return adapt(f_eq60, a_new, b_new, acc, eps, err);
}
