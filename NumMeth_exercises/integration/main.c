#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_integration.h>
#include"integration.h"


int main(void){
	printf("Part A initializing.\n\n");
	printf("A recursive adaptive integrator is implemented. With a given accuracy goal and lower order method based error-claculation, it uses recursive segmentation of the integral section in order to achieve the given accuracy goal. Points are saved and reused for each recursion.\n");
	printf("The integrator is tested on the integrals provided in the exercise description:\n");


	/*First integral*/
	printf("\nFirst test: I = ∫ from 0 to 1 of √(x) dx = 2/3\n");
	int calls_A1 = 0;
	double err_A1 = 0, a=0, b=1, acc=1e-4, eps=1e-4;
	double f1(double x){calls_A1++; return sqrt(x);};
	printf("The integrator is called.\n");
	double I_A1 = adapt(f1, a, b, acc, eps, &err_A1);
	printf("The integrator is done calculating.\n");
	printf("The integrator returned I=%g, error=%g, calls=%i\n\n",I_A1,err_A1, calls_A1);

	/*Second integral*/
	printf("Second test: I = ∫ from 0 to 1 of 1/√(x) dx = 2\n");
	int calls_A2 = 0;
	double err_A2 = 0;
	a=0, b=1, acc=1e-4, eps=1e-4;
	double f2(double x){calls_A2++; return 1.0/sqrt(x);};
	printf("The integrator is called.\n");
	double I_A2 = adapt(f2, a, b, acc, eps, &err_A2);
	printf("The integrator is done calculating.\n");
	printf("The integrator returned I=%g, error=%g, calls=%i\n\n",I_A2,err_A2, calls_A2);


	/*Third integral*/
	printf("Third test: I = ∫ from 0 to 1 of ln(X)/√(x) dx = -4\n");
	int calls_A3 = 0;
	double err_A3 = 0;
	a=0, b=1, acc=1e-4, eps=1e-4;
	double f3(double x){calls_A3++; return log(x)/sqrt(x);};
	printf("The integrator is called.\n");
	double I_A3 = adapt(f3, a, b, acc, eps, &err_A3);
	printf("The integrator is done calculating.\n");
	printf("The integrator returned I=%g, error=%g, calls=%i\n\n",I_A3,err_A3, calls_A3);
	
	printf("The integrator handled all three integrals. It is clear that the number of recursive calls vastly increases if there are diverging terms in the integral. This makes sense, as the rapidly diverging terms quickly contribute inaccuracy if small enough steps are not taken.\n");


	/*Pi integral*/
	printf("\nAs a testament to the accuracy of the integrator, we were asked to do the following integral with the maximal number of decimals:\n");
	printf("I = ∫ from 0 to 1 of 4 *√(1-(1-x)^2) dx = π\n");
        int calls_A4 = 0;
        double err_A4 = 0;
        double pi_a=0, pi_b=1, pi_acc=1e-19, pi_eps=1e-19;
        double f4(double x){calls_A4++; return 4*sqrt(1.0-(1.0-x)*(1.0-x));};
        printf("The integrator is called.\n");
        double I_A4 = adapt(f4, pi_a, pi_b, pi_acc, pi_eps, &err_A4);
        printf("The integrator is done calculating.\n");
	printf("Pi is\t=3.141592653589793238462\n");
	printf("I\t=%23.23g\n",I_A4);
        printf("The integrator also returned error=%g, calls=%i\n\n",err_A4, calls_A4);
	printf("The error is of a size of the order of the machine epsilon.\n");

	printf("This concludes part A of the exercise.\n");

	
	/*Part B*/
	printf("\nPart B now running.\n");
	printf("A version of the adaptive integrator using the Clenshaw-Curtis variable transform has been implemented.\n");
	printf("This variable transform can sometimes help with accuracy, if the function is particularly suited. Good examples could be some functions diverging at the end of integration intervals. The integrator is compared to the GSL integrator using Gauss-Kronrod 21 QAGS.\n\n");

	/*First integral*/
	printf("Test 1 from A: I = ∫ from 0 to 1 of √(x) dx = 2/3\n" );
	int calls_B1 = 0;
	double err_B1 = 0;
	double a_B = 0, b_B = 1, acc_B = 1e-4, eps_B = 1e-4;
	double f_B1(double x){calls_B1++; return sqrt(x);};
	printf("Calling the integrator.\n");
	double I_B1 = adapt_clenshaw_curtis(f_B1,a_B,b_B,acc_B,eps_B,&err_B1);
	printf("The calculation is done.\n");

	int calls_B11 = 0;
	int limit=100;
	gsl_integration_workspace* w;
	w = gsl_integration_workspace_alloc(limit);
	double f_B11(double x, void* params){calls_B11++; return sqrt(x);};
	gsl_function F;
	F.function = f_B11;
	double I_B11, err_B11;
	gsl_integration_qags(&F, 0, 1, acc_B, eps_B, limit, w, &I_B11, &err_B11);
	gsl_integration_workspace_free(w);

	printf("Previous result:\tI=%lg, error=%lg, calls=%i\n",I_A1,err_A1,calls_A1);
	printf("Clenshaw-Curtis:\tI=%lg, error=%lg, calls=%i\n",I_B1,err_B1,calls_B1);
	printf("GSL qags:\t\tI=%lg, error=%lg, calls=%i\n",I_B11,err_B11,calls_B11);


	/*Second integral*/
	printf("\nTest 2 from A: I = ∫ from 0 to 1 of 1/√(x) dx = 2 \n");
        int calls_B2 = 0;
        double err_B2 = 0;
        a_B = 0, b_B = 1, acc_B = 1e-4, eps_B = 1e-4;
        double f_B2(double x){calls_B2++; return 1.0/sqrt(x);};
        printf("Calling the integrator.\n");
        double I_B2 = adapt_clenshaw_curtis(f_B2,a_B,b_B,acc_B,eps_B,&err_B2);
        printf("The calculation is done.\n");

        int calls_B22 = 0;
        limit=100;
        gsl_integration_workspace* w2;
        w2 = gsl_integration_workspace_alloc(limit);
        double f_B22(double x, void* params){calls_B22++; return 1.0/sqrt(x);};
        gsl_function F2;
        F2.function = f_B22;
        double I_B22, err_B22;
        gsl_integration_qags(&F2, 0, 1, acc_B, eps_B, limit, w, &I_B22, &err_B22);
        gsl_integration_workspace_free(w2);

        printf("Previous result:\tI=%lg, error=%lg, calls=%i\n",I_A2,err_A2,calls_A2);
        printf("Clenshaw-Curtis:\tI=%lg, error=%lg, calls=%i\n",I_B2,err_B2,calls_B2);
        printf("GSL qags:\t\tI=%lg, error=%lg, calls=%i\n",I_B22,err_B22,calls_B22);


        /*Third integral*/
        printf("\nTest 3 from A: I = ∫ from 0 to 1 of ln(x)/√(x) dx = -4 \n");
        int calls_B3 = 0;
        double err_B3 = 0;
        a_B = 0, b_B = 1, acc_B = 1e-4, eps_B = 1e-4;
        double f_B3(double x){calls_B3++; return log(x)/sqrt(x);};
        printf("Calling the integrator.\n");
        double I_B3 = adapt_clenshaw_curtis(f_B3,a_B,b_B,acc_B,eps_B,&err_B3);
        printf("The calculation is done.\n");

        int calls_B33 = 0;
        limit=100;
        gsl_integration_workspace* w3;
        w3 = gsl_integration_workspace_alloc(limit);
        double f_B33(double x, void* params){calls_B33++; return log(x)/sqrt(x);};
        gsl_function F3;
        F3.function = f_B33;
        double I_B33, err_B33;
        gsl_integration_qags(&F3, 0, 1, acc_B, eps_B, limit, w, &I_B33, &err_B33);
        gsl_integration_workspace_free(w3);

        printf("Previous result:\tI=%lg, error=%lg, calls=%i\n",I_A3,err_A3,calls_A3);
        printf("Clenshaw-Curtis:\tI=%lg, error=%lg, calls=%i\n",I_B3,err_B3,calls_B3);
        printf("GSL qags:\t\tI=%lg, error=%lg, calls=%i\n",I_B33,err_B33,calls_B33);


	/*Calculation of pi*/
	printf("\nThe fourth test is the precision test finding π from the integral\n");
	printf("I = ∫ from 0 to 1 of 4 *√(1-(1-x)2) dx = π \n");

	int calls_B4 = 0;
	double err_B4 = 0;
	double a_B4 = 0, b_B4 = 1, acc_B4=1e-19, eps_B4 = 1e-19;
	double f_B4(double x){calls_B4++; return 4.*sqrt(1.0-(1.0-x)*(1.0-x));};
	printf("Running integrator.\n");
	double I_B4 = adapt_clenshaw_curtis(f_B4, a_B4, b_B4, acc_B4,eps_B4, &err_B4);
	printf("The integrator finished.\n");

	int calls_B44 = 0;
	double acc_B44 = 1e-19, eps_B44 = 1e-10;
	int limit_B44 = 10000;
	gsl_integration_workspace* w4;
	w4 = gsl_integration_workspace_alloc(limit_B44);
	double f_B44(double x, void* params){calls_B44++; return 4.0*sqrt(1.0 - (1.0 - x)*(1.0 -x));};
	gsl_function F4;
	F4.function = f_B44;
	double I_B44, err_B44;
	gsl_integration_qags(&F4, 0, 1, acc_B44, eps_B44, limit_B44, w4, &I_B44, &err_B44);
	gsl_integration_workspace_free(w4);
	
	printf("Known solution:\t\tπ = 3.141592653589793238462\n");
	printf("From A :\t\tI = %23.23g\n",I_A4);
	printf("Clenshaw-Curtis:\tI = %23.23g\n",I_B4);
	printf("GSL qags :\t\tI = %23.23g\n",I_B44);
	printf("Part A\t\terror = %g and %i calls \n",err_A4,calls_A4);
	printf("Clenshaw-Curtis\terror = %g and %i calls \n",err_B4,calls_B4);
	printf("GSL qags\terror = %g and %i calls\n",err_B44,calls_B44);

	printf("For the diverging functions Clenshaw-Curtis is most efficient. The GSL qags routine in general needs much fewer function calls to arrive at approximately the same result. A better suited GSL integration routine would likely beat my naive Clenshaw-Curtis routine. There are many different GSL integration routines, each slightly better suited for its own niche of integrals. This exercise proves that which integrator is fastest and most efficient depends on the specific integral being solved.\n");

	printf("\nThis concludes part B of the exercise.\n");


	/*Part C*/

	printf("\nPart C initializing.\n");
	printf("An adaptive integrator utilizing a variable transformation to integrate from intial point 'a' to infinity has been implemented. To compare, the same integrals are calculated using the GSL routine gsl_integration_qagiu which implements the same variable transformation.\n");


	printf("The first test I decided on was the integral\nI = ∫ from 0 to inf of exp(-x*x) dx = √π/2 = 0.886226925452758...\n");

	int calls_inf1 = 0;
	double err_inf1 = 0;
	double a_inf1 = 0, acc_inf1 = 1e-12, eps_inf1 = 1e-12;

	double f_inf1(double x){calls_inf1++; return exp(-x*x);};

	printf("Integrator running.\n");
	double I_inf1 = adapt_a_to_infinity(f_inf1, a_inf1, acc_inf1,eps_inf1, &err_inf1);
	printf("Integration done.\n");

	int calls_inf11 = 0;
	int limit_inf11 = 10000;
	gsl_integration_workspace* w_inf11;
	w_inf11 = gsl_integration_workspace_alloc(limit_inf11);
	double f_inf11(double x, void* params){calls_inf11++; return exp(-x*x);};
	gsl_function F_inf11;
	F_inf11.function = f_inf11;
	double I_inf11, err_inf11;
	gsl_integration_qagiu(&F_inf11, a_inf1, acc_inf1, eps_inf1, limit_inf11, w_inf11, &I_inf11, &err_inf11);
	gsl_integration_workspace_free(w_inf11);

	printf("Analytical solution is\tI=0.886226925452758..\n");
	printf("My own integrator gave \tI=%12.12g with error =%g in %i steps.\n",I_inf1,err_inf1,calls_inf1);
	printf("The GSL rouinte gave \tI=%12.12g with error =%g in %i steps.\n",I_inf11,err_inf11,calls_inf11);

	
	printf("\nThe next test is the integral I = ∫ from 0 to inf of √x/(x+1)^2 dx = π/2.\n");

	int calls_inf2 = 0;
        double err_inf2 = 0;
        double a_inf2 = 0, acc_inf2 = 1e-12, eps_inf2 = 1e-12;

        double f_inf2(double x){calls_inf2++; return sqrt(x)/((x+1)*(x+1));};

        printf("Integrator running.\n");
        double I_inf2 = adapt_a_to_infinity(f_inf2, a_inf2, acc_inf2,eps_inf2, &err_inf2);
        printf("Integration done.\n");

        int calls_inf22 = 0;
        int limit_inf22 = 10000;
        gsl_integration_workspace* w_inf22;
        w_inf22 = gsl_integration_workspace_alloc(limit_inf22);
        double f_inf22(double x, void* params){calls_inf22++; return sqrt(x)/((x+1)*(x+1));};
        gsl_function F_inf22;
        F_inf22.function = f_inf22;
        double I_inf22, err_inf22;
        gsl_integration_qagiu(&F_inf22, a_inf2, acc_inf2, eps_inf2, limit_inf22, w_inf22, &I_inf22, &err_inf22);
        gsl_integration_workspace_free(w_inf22);

        printf("Analytical solution is\tI=1.570796326794896619...\n");
        printf("My own integrator gave \tI=%12.22g with error =%g in %i steps.\n",I_inf2,err_inf2,calls_inf2);
        printf("The GSL rouinte gave \tI=%12.22g with error =%g in %i steps.\n",I_inf22,err_inf22,calls_inf22);

	printf("\nIt is clear that the GSL routine uses far fewer steps than my naive implementation. It is however also clear, that my implementation reaches about the same level of precision. Thus having implemented the adaptive integrator integrating from a to infinity, part C of the exercise is done.\n");

	printf("\n\nThis concludes part C of the exercise and thus the entire exercise.\n"); 

	return 0;
}
