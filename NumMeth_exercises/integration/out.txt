Part A initializing.

A recursive adaptive integrator is implemented. With a given accuracy goal and lower order method based error-claculation, it uses recursive segmentation of the integral section in order to achieve the given accuracy goal. Points are saved and reused for each recursion.
The integrator is tested on the integrals provided in the exercise description:

First test: I = ∫ from 0 to 1 of √(x) dx = 2/3
The integrator is called.
The integrator is done calculating.
The integrator returned I=0.666681, error=3.32605e-05, calls=40

Second test: I = ∫ from 0 to 1 of 1/√(x) dx = 2
The integrator is called.
The integrator is done calculating.
The integrator returned I=2, error=1.19752e-05, calls=17140

Third test: I = ∫ from 0 to 1 of ln(X)/√(x) dx = -4
The integrator is called.
The integrator is done calculating.
The integrator returned I=-4, error=3.27216e-05, calls=29972

The integrator handled all three integrals. It is clear that the number of recursive calls vastly increases if there are diverging terms in the integral. This makes sense, as the rapidly diverging terms quickly contribute inaccuracy if small enough steps are not taken.

As a testament to the accuracy of the integrator, we were asked to do the following integral with the maximal number of decimals:
I = ∫ from 0 to 1 of 4 *√(1-(1-x)^2) dx = π
The integrator is called.
The integrator is done calculating.
Pi is	=3.141592653589793238462
I	=3.141592653589793115998
The integrator also returned error=0, calls=127191304

The error is of a size of the order of the machine epsilon.
This concludes part A of the exercise.

Part B now running.
A version of the adaptive integrator using the Clenshaw-Curtis variable transform has been implemented.
This variable transform can sometimes help with accuracy, if the function is particularly suited. Good examples could be some functions diverging at the end of integration intervals. The integrator is compared to the GSL integrator using Gauss-Kronrod 21 QAGS.

Test 1 from A: I = ∫ from 0 to 1 of √(x) dx = 2/3
Calling the integrator.
The calculation is done.
Previous result:	I=0.666681, error=3.32605e-05, calls=40
Clenshaw-Curtis:	I=0.666667, error=4.49565e-06, calls=64
GSL qags:		I=0.666667, error=7.734e-05, calls=189

Test 2 from A: I = ∫ from 0 to 1 of 1/√(x) dx = 2 
Calling the integrator.
The calculation is done.
Previous result:	I=2, error=1.19752e-05, calls=17140
Clenshaw-Curtis:	I=2, error=1.03007e-05, calls=52
GSL qags:		I=2, error=5.77316e-15, calls=231

Test 3 from A: I = ∫ from 0 to 1 of ln(x)/√(x) dx = -4 
Calling the integrator.
The calculation is done.
Previous result:	I=-4, error=3.27216e-05, calls=29972
Clenshaw-Curtis:	I=-4, error=3.8707e-06, calls=272
GSL qags:		I=-4, error=1.35447e-13, calls=315

The fourth test is the precision test finding π from the integral
I = ∫ from 0 to 1 of 4 *√(1-(1-x)2) dx = π 
Running integrator.
The integrator finished.
Known solution:		π = 3.141592653589793238462
From A :		I = 3.141592653589793115998
Clenshaw-Curtis:	I = 3.141592653589793115998
GSL qags :		I = 3.141592653589793115998
Part A		error = 0 and 127191304 calls 
Clenshaw-Curtis	error = 1.43231e-22 and 183521748 calls 
GSL qags	error = 5.30656e-11 and 273 calls
For the diverging functions Clenshaw-Curtis is most efficient. The GSL qags routine in general needs much fewer function calls to arrive at approximately the same result. A better suited GSL integration routine would likely beat my naive Clenshaw-Curtis routine. There are many different GSL integration routines, each slightly better suited for its own niche of integrals. This exercise proves that which integrator is fastest and most efficient depends on the specific integral being solved.

This concludes part B of the exercise.

Part C initializing.
An adaptive integrator utilizing a variable transformation to integrate from intial point 'a' to infinity has been implemented. To compare, the same integrals are calculated using the GSL routine gsl_integration_qagiu which implements the same variable transformation.
The first test I decided on was the integral
I = ∫ from 0 to inf of exp(-x*x) dx = √π/2 = 0.886226925452758...
Integrator running.
Integration done.
Analytical solution is	I=0.886226925452758..
My own integrator gave 	I=0.886226925453 with error =6.31594e-15 in 127516 steps.
The GSL rouinte gave 	I=0.886226925453 with error =5.15333e-13 in 195 steps.

The next test is the integral I = ∫ from 0 to inf of √x/(x+1)^2 dx = π/2.
Integrator running.
Integration done.
Analytical solution is	I=1.570796326794896619...
My own integrator gave 	I=1.570796326794896557999 with error =7.61258e-18 in 15725100 steps.
The GSL rouinte gave 	I=1.570796326794895891865 with error =2.64455e-13 in 525 steps.

It is clear that the GSL routine uses far fewer steps than my naive implementation. It is however also clear, that my implementation reaches about the same level of precision. Thus having implemented the adaptive integrator integrating from a to infinity, part C of the exercise is done.


This concludes part C of the exercise and thus the entire exercise.
