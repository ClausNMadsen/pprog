#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<math.h>
#include"qr_gs.h"

int newton_w_H(double f(gsl_vector* x, gsl_vector* fx, gsl_matrix* H), gsl_vector* x, double eps){

	int n = x->size;
	int step = 0;

	gsl_matrix* H = gsl_matrix_alloc(n,n);
	gsl_matrix* R = gsl_matrix_alloc(n,n);
	gsl_vector* grad_f = gsl_vector_alloc(n);
	gsl_vector* dx = gsl_vector_alloc(n);

	f(x, grad_f, H); /*Update Hessen matrix and grad_f*/
	double norm_grad_f = gsl_blas_dnrm2(grad_f);

	while(norm_grad_f > eps){
		f(x, grad_f, H); /*first update*/
		
		qr_gs_decomp(H,R); /* then solve grad(phi) + H(x) = 0 for dx*/
		qr_gs_solve(H, R, grad_f, dx);

		double lambda = 1;
		double fx = f(x,grad_f,H);
		gsl_vector_scale(dx,-lambda);
		gsl_vector_add(x,dx);
		gsl_vector_scale(dx,-1.00/lambda);

		double dotprod; /*implementing Armijo condition*/
		gsl_blas_ddot(dx,grad_f, &(dotprod));
		while(f(x,grad_f,H) > (fx +1e-5*lambda*dotprod) && lambda > 1.00/64.00){
			lambda /= 2.00;
			gsl_vector_scale(dx, lambda);
			gsl_vector_add(x,dx);
			gsl_vector_scale(dx, 1.00/lambda);
		}

		f(x, grad_f, H);
		norm_grad_f = gsl_blas_dnrm2(grad_f);
		step++;
	}

	gsl_matrix_free(H);
	gsl_matrix_free(R);
	gsl_vector_free(dx);
	gsl_vector_free(grad_f);
	return step;
}


