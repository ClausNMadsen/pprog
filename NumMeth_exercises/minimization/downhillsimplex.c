#include<math.h>
#include"downhillsimplex.h"
/*The function is ripped directly from Dmitri's lecture notes on Downhill Simplex*/

void reflection(double* highest, double* centroid, int dim, double* reflected){
	/*The highest point is reflected against the centroid*/
	for(int i = 0; i < dim; i++){
		reflected[i] = 2*centroid[i]-highest[i];
	}
}

void expansion(double* highest, double* centroid, int dim, double* expanded){
	/*The highest point is reflected and extended to double its distance from the centroid*/
	for(int i = 0; i < dim; i++){
		expanded[i] = 3*centroid[i]-2*highest[i];
	}
}

void contraction(double* highest, double* centroid, int dim, double* contracted){
	/*The highest point halves its distance from the centroid*/
	for(int i = 0; i < dim; i++){
		contracted[i] = 0.5*(centroid[i]+highest[i]);
	}
}

void reduction(double** simplex, int dim, int lo){
	/*middle and highest points move towards the lowest point, halving their distances*/
	for(int k = 0; k < dim +1; k++){
		if(k != lo){
			for(int i = 0; i<dim; i++){
				simplex[k][i] = 0.5*(simplex[k][i]+simplex[lo][i]);
			}
		}
	}
}

double distance(double* a, double* b, int dim){
	/*Finds the distance a and b in arbitrary dimension*/
	double s = 0;
	for(int i = 0; i < dim; i++){
		s += pow(a[i] - b[i],2);
	}
	return sqrt(s);
}

double size(double** simplex, int dim){
	/*Check for the longest distance between first and last point*/
	double s = 0;
	for(int k = 1; k < dim+1; k++){
		double dist=distance(simplex[0],simplex[k],dim);
		if(dist>s){
			s = dist;
		}
	}
	return s;
}

void simplex_update(double** simplex, double* f_values, int d, int* hi, int* lo, double* centroid){
	/*Find the highest, lowest and centroid points of the simplex*/
	*hi = 0;
	double highest = f_values[0];
	*lo = 0;
	double lowest = f_values[0];

	for(int k = 1; k<d+1; k++){
		double next = f_values[k];
		if(next > highest){
			highest = next;
			*hi = k;
		}
		if(next < lowest){
			lowest = next;
			*lo = k;
		}
	}
	for(int i = 0; i<d; i++){
		double sum = 0;
		for(int k=0; k<d+1; k++){
			if(k!=*hi){
				sum += simplex[k][i];
			}
			centroid[i] = sum/d;
		}
	}
}

void simplex_initiate(double (*fun)(double*), double** simplex, double* f_values, int d, int* hi, int* lo, double* centroid){
	/*Initiate simplex, then update*/
	for(int k=0; k<d+1; k++){
		f_values[k] = fun(simplex[k]);
	}
	simplex_update(simplex,f_values,d,hi,lo,centroid);
}

int downhill_simplex(double F(double*), double** simplex, int d, double simplex_size_goal){
	int hi, lo, k=0;
	double centroid[d], f_value[d+1], p1[d], p2[d];

	/*initiate*/
	simplex_initiate(F,simplex,f_value,d,&hi,&lo,centroid);

	/*iterate procedure until simplex size goal is reached*/
	while(size(simplex,d) > simplex_size_goal){
		simplex_update(simplex,f_value,d,&hi,&lo,centroid);

		/*try reflection*/
		reflection(simplex[hi],centroid,d,p1);

		double f_re = F(p1);

		if(f_re<f_value[lo]){/*if reflection seems good, check expansion*/
			expansion(simplex[hi],centroid,d,p2);
			double f_ex = F(p2);
			if (f_ex<f_re){/*if expansion looks good, accept it*/
				for(int i = 0; i<d; i++){
					simplex[hi][i] = p2[i];
					f_value[hi]=f_ex;
				}
			}
			else{/*reflection is good, expansion is bad, just reflect*/
				for(int i = 0; i<d; i++){
					simplex[hi][i] = p1[i];
					f_value[hi] = f_re;
				}
			}
		}
		else{/*if reflection looks bad*/
			if(f_re<f_value[hi]){
				for(int i = 0; i<d; i++){
					simplex[hi][i] = p1[i];
					f_value[hi] = f_re;
				}
			}
			else{/*try contraction*/
				contraction(simplex[hi],centroid,d,p1);
				double f_co=F(p1);
				if(f_co<f_value[hi]){/*if contraction looks good, accept*/
					for(int i = 0; i<d; i++){
						simplex[hi][i]=p1[i];
						f_value[hi]=f_co;
					}
				}
				else{/*if neither reflection nor contraction works, reduct*/
					reduction(simplex,d,lo);
					simplex_initiate(F,simplex,f_value,d,&hi,&lo,centroid);
				}
			}
		}
		k++;
	}
	return k;
}
