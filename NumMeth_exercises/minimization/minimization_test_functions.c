#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

double rosenbrock_w_H(gsl_vector* x, gsl_vector* y, gsl_matrix* H){
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 2*(x_1-1) - 400*x_1*(x_2-x_1*x_1));
	gsl_vector_set(y,1, 200*x_2 - 200*x_1*x_1);
	double H_11 = 2 - 400*x_2 + 1200*x_1*x_1;
	double H_12 = -400*x_1;
	double H_21 = -400*x_1;
	double H_22 = 200;
	gsl_matrix_set(H, 0, 0, H_11);
	gsl_matrix_set(H, 0, 1, H_12);
	gsl_matrix_set(H, 1, 0, H_21);
	gsl_matrix_set(H, 1, 1, H_22);
	return (1-x_1)*(1-x_1) + 100*(x_2 - x_1*x_1)*(x_2-x_1*x_1);
}

double himmel_w_H(gsl_vector* x, gsl_vector* y, gsl_matrix* H){
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 4*x_1*(x_1*x_1 + x_2 - 11) + 2*(x_1 + x_2*x_2 - 7));
	gsl_vector_set(y,1, 2*(x_1*x_1 + x_2 - 11) + 4*x_2*(x_1 + x_2*x_2 - 7));
	double H_11 = 12*x_1*x_1 + 4*x_2 - 42;
	double H_12 = 4*(x_1 + x_2);
	double H_21 = 4*(x_1 + x_2);
	double H_22 = 4*x_1 + 12*x_2*x_2 - 26;
	gsl_matrix_set(H, 0, 0, H_11);
	gsl_matrix_set(H, 0, 1, H_12);
	gsl_matrix_set(H, 1, 0, H_21);
	gsl_matrix_set(H, 1, 1, H_22);
	return (x_1*x_1 + x_2 - 11)*(x_1*x_1 + x_2 - 11) + (x_1 + x_2*x_2 - 7)*(x_1 + x_2*x_2 -7);
}

double rosenbrock_wo_H(gsl_vector* x, gsl_vector* y){
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 2*(x_1 -1) - 400*x_1*(x_2 - x_1*x_1));
	gsl_vector_set(y,1, 200*x_2 - 200*x_1*x_1);
	return (1 - x_1)*(1 - x_1) + 100*(x_2 - x_1*x_1)*(x_2 - x_1*x_1);
}

double himmel_wo_H(gsl_vector* x, gsl_vector* y){
	double x_1 = x->data[0], x_2 = x-> data[1];
	gsl_vector_set(y,0, 4*x_1*(x_1*x_1 + x_2 - 11) + 2*(x_1 + x_2*x_2 - 7));
	gsl_vector_set(y,1, 2*(x_1*x_1 + x_2 - 11) + 4*x_2*(x_1 + x_2*x_2 - 7));
	return (x_1*x_1 + x_2 - 11)*(x_1*x_1 + x_2 - 11) + (x_1 + x_2*x_2 -7)*(x_1 + x_2*x_2 -7);
}

double function_fit_wo_H(gsl_vector* x, gsl_vector* grad){
	double A = x->data[0], T = x->data[1], B = x->data[2];

	/*hard-coding data*/
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double func = 0, data = 0, mismatch = 0;

	for(int i=0; i<N; i++){
		func = A*exp(-t[i]/T)+B;
		data = y[i];
		mismatch += pow((func - data)/(e[i]),2);
	}

	/*Analytical gradient*/
	double difffuncA = 0, difffuncT = 0, difffuncB = 0;
	double diffeqA = 0, diffeqB = 0, diffeqT = 0;

	for(int i=0; i<N; i++){
		func = A*exp(-t[i]/T)+B;
		data = y[i];
		difffuncA = exp(-t[i]/T);
		diffeqA = diffeqA + 2*(func - data)*difffuncA/(e[i]*e[i]);
	}

	for(int i=0;i<N;i++){
		func = A*exp(-t[i]/T)+B;
		data = y[i];
		difffuncT = A*t[i]*exp(-t[i]/T)/(T*T);
		diffeqT = diffeqT + 2*(func-data)*difffuncT/(e[i]*e[i]);
	}

	for(int i=0;i<N;i++){
		func = A*exp(-t[i]/T)+B;
		data = y[i];
		difffuncB = 1;
		diffeqB = diffeqB + 2*(func-data)*difffuncB/(e[i]*e[i]);
	}

	gsl_vector_set(grad,0,diffeqA);
	gsl_vector_set(grad,1,diffeqT);
	gsl_vector_set(grad,2,diffeqB);

	return mismatch;
}

double rosenbrock_simple(double* x){
	double x_1 = x[0], x_2 = x[1];
	return (1-x_1)*(1-x_1) + 100*(x_2 - x_1*x_1)*(x_2 - x_1*x_1);
}

double himmel_simple(double* x){
	double x_1=x[0], x_2=x[1];
	return (x_1*x_1 + x_2 - 11)*(x_1*x_1 + x_2 - 11) + (x_1 + x_2*x_2 - 7)*(x_1 + x_2*x_2 - 7);
}

double fit_simple(double* x){
	double A=x[0], T=x[1], B=x[2];

	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	double func = 0, data = 0, mismatch = 0;

	for(int i=0; i<N; i++){
		func = A*exp(-t[i]/T)+B;
		data = y[i];
		mismatch += pow((func-data)/(e[i]),2);
	}
	return mismatch;
}
