#ifndef HAVE_MINIMIZATION_TEST_FUNCTIONS_H
#define HAVE_MINIMIZATION_TEST_FUNCTIONS_H
double rosenbrock_w_H(gsl_vector* x, gsl_vector* y, gsl_matrix* H);
double himmel_w_H(gsl_vector* x, gsl_vector* y, gsl_matrix* H);
double rosenbrock_wo_H(gsl_vector* x, gsl_vector* y);
double himmel_wo_H(gsl_vector* x, gsl_vector* y);
double function_fit_wo_H(gsl_vector* x, gsl_vector* grad);
double rosenbrock_simple(double* x);
double himmel_simple(double* x);
double fit_simple(double* x);
#endif
