#ifndef HAVE_MINIMIZATION_H
#define HAVE_MINIMIZATION_H
#include"minimization_test_functions.h"
#include"newton_w_H.h"
#include"quasi_newton_w_H.h"
#include"newton_w_jacobian.h"
#include"root_test_functions.h"
#endif
