#ifndef NEWTON_W_HESSIAN_H
#define NEWTON_W_HESSIAN_H
int newton_w_H(double f(gsl_vector* x, gsl_vector* fx, gsl_matrix* H), gsl_vector* x, double eps);
#endif
