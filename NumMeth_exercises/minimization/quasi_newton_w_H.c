#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_errno.h>
#include"quasi_newton_w_H.h"

int quasi_newton_w_H(double f(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double eps){
	
	double dx = 1e-4;
	int n = x->size, step = 0;

	gsl_matrix* H_inv = gsl_matrix_alloc(n,n);
	gsl_matrix* extra_mem = gsl_matrix_alloc(n,n);
	gsl_vector* grad_f = gsl_vector_alloc(n);
	gsl_vector* grad_f_dx = gsl_vector_alloc(n);
	gsl_vector* delta_x = gsl_vector_alloc(n);


	gsl_matrix_set_identity(H_inv);

	double fx = f(x, grad_f);

	do{
		fx = f(x,grad_f);
		gsl_blas_dgemv(CblasNoTrans,1.0,H_inv,grad_f,0.0,delta_x);
		double lambda = 1.0;

		gsl_vector_scale(delta_x, -lambda);
		gsl_vector_add(x, delta_x);
		gsl_vector_scale(delta_x, -1.00/lambda);

		double dotprod;
		gsl_blas_ddot(delta_x, grad_f, &dotprod);
		while(f(x,grad_f_dx) > (fx + 1e-4*lambda*dotprod) && lambda > dx/10){
			lambda /= 2.00;
			gsl_vector_scale(delta_x, lambda);
			gsl_vector_add(x,delta_x);
			gsl_vector_scale(delta_x, 1.00/lambda);
		}

		f(x, grad_f_dx);
		double norm_grad_f_dx = gsl_blas_dnrm2(grad_f_dx);
		step++;


		if(norm_grad_f_dx < eps){ break;}
		else {
			gsl_vector_scale(grad_f_dx, -1);
			gsl_vector_add(grad_f, grad_f_dx);
			gsl_vector_scale(grad_f, -1);
			H_broyden_update(H_inv, extra_mem, grad_f, delta_x, lambda);
			gsl_vector_memcpy(grad_f_dx, grad_f);
		}
	}while(step <1e5);

	gsl_matrix_free(H_inv);
	gsl_matrix_free(extra_mem);
	gsl_vector_free(delta_x);
	gsl_vector_free(grad_f);
	gsl_vector_free(grad_f_dx);

	return step;
}

int H_broyden_update(gsl_matrix* H_inv, gsl_matrix* H_new, gsl_vector* y, gsl_vector* s, double lambda){

	int n = H_inv ->size1;
	gsl_vector* product = gsl_vector_alloc(n);
	gsl_vector* H_inv_y = gsl_vector_alloc(n);
	gsl_vector* parant = gsl_vector_alloc(n);

	/* Broyden step: H_inv -> H_inv + (s-H_inv y)*s^T*H_inv / (y^T*H_inv s)*/
	gsl_vector_scale(s, -lambda);
	double y_trans_H_inv_s = 0.0;
	gsl_blas_dgemv(CblasNoTrans,1.0,H_inv,s,0.0,product);
	gsl_blas_ddot(y,product,&y_trans_H_inv_s);

	/*Diverging updates causes a reset of the inverse Hessian to unity*/
	if(fabs(y_trans_H_inv_s) < 1e-3){ gsl_matrix_set_identity(H_inv); return 0;}

	gsl_blas_dgemv(CblasNoTrans,1.0,H_inv,y,0.0,H_inv_y);
	gsl_vector_memcpy(parant,s);
	gsl_vector_sub(parant,H_inv_y);
	gsl_blas_dger(1.00/y_trans_H_inv_s,parant,product,H_inv);
	gsl_matrix_memcpy(H_new, H_inv);

	gsl_vector_free(product);
	gsl_vector_free(H_inv_y);
	gsl_vector_free(parant);
	return 0;
}
