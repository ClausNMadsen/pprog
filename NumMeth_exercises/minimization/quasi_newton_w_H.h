#ifndef HAVE_QUASI_NEWTON_W_H_H
#define HAVE_QUASI_NEWTON_W_H_H
int H_broyden_update(gsl_matrix* H_inv, gsl_matrix* H_new, gsl_vector* y, gsl_vector* s, double lambda);
int quasi_newton_w_H(double f(gsl_vector* x, gsl_vector* fx), gsl_vector* x, double eps);
#endif
