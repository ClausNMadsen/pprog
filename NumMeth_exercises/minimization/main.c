#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_errno.h>
#include"minimization.h"
#include"downhillsimplex.h"

int main (void){

	/*Exercise A */
	printf("Part A initializing.\n");
	printf("Newton's method with back-tracking linesearch with user provided analytical gradient and Hessian matrix is implemented.\n");


	/*Allocating vectors for part A*/
	gsl_vector *x_rosen_A = gsl_vector_alloc (2);
	gsl_vector *x_himmel_A = gsl_vector_alloc (2);


	/*Rosenbrock's valley function*/
	printf("\nThe first test of the implementation is finding the minimum of Rosenbrock's valley function.\n");
	gsl_vector_set (x_rosen_A, 0, 0);
	gsl_vector_set (x_rosen_A, 1, 0);
	printf("Using initial point x=(%g,%g).\n", x_rosen_A->data[0],x_rosen_A->data[1]);
	int step_rosen_A = newton_w_H (&rosenbrock_w_H, x_rosen_A, 1e-6);
	printf ("Newton's method used %i steps.\n", step_rosen_A);
	printf("The local minimum is supposed to be at x=(1,1). Newton's method found x=(%g,%g).\n",x_rosen_A->data[0], x_rosen_A->data[1]);


	/*Himmelblau's function*/
	printf("\nThe second test is Himmelblau's function.\n");
	gsl_vector_set (x_himmel_A, 0, 2);
	gsl_vector_set (x_himmel_A, 1, 2);
	printf ("Using initial point x=(%g,%g).\n", x_himmel_A->data[0],x_himmel_A->data[1]);
	int step_himmel_A = newton_w_H (&himmel_w_H, x_himmel_A, 1e-6);
	printf("Newton's method used %i steps.\n", step_himmel_A);
	printf("The four target local minima are x=(3.0,2.0), x=(-2.805118,3.131312), x=(-3.779310,-3.283186) and x=(3.584428,-1.848126).\n");
	printf("Newton's method converged at the point x=(%g,%g).\n",x_himmel_A->data[0], x_himmel_A->data[1]);
	printf("This minimization algorithm might also find a local maximum instead of a local minimum, as the gradient is also zero at a local maximum.\n");
	printf ("\nThis concludes part A of this exercise.\n\n\n");


	/*Part B*/
	printf ("Part B initializing.\n");
	printf("A modified quasi Newton's method with back-tracking linesearch, user provided analytical gradient and analytical Hessian matrix implementing Broyden's update is implemented.\n");
	printf("The performance of this method is compared to the newton root-finding procedure with user-provided analytical Jacobian from the root-finding exercise.\n");

	/*parameters*/
	gsl_vector *x_rosen_quasi = gsl_vector_alloc (2);
	gsl_vector *x_rosen_root = gsl_vector_alloc (2);
	gsl_vector *x_himmel_quasi = gsl_vector_alloc (2);
	gsl_vector *x_himmel_root = gsl_vector_alloc (2);
	gsl_vector *f_call = gsl_vector_alloc (3);

	/*Rosenbrock's valley function*/

	printf ("\nAgain, the first test is Rosenbrock's valley function.\n");
	gsl_vector_set (x_rosen_quasi, 0, 0);
	gsl_vector_set (x_rosen_quasi, 1, 0);
	gsl_vector_set (x_rosen_root, 0, 0);
	gsl_vector_set (x_rosen_root, 1, 0);
	int step_rosen_quasi = quasi_newton_w_H (&rosenbrock_wo_H, x_rosen_quasi, 1e-6);
	gsl_vector_set (f_call, 1, 0);
	int step_rosen_root = newton_w_jacobian (rosenbrock_valley_with_J, x_rosen_root, 1e-6, f_call);
	printf("The quasi Newton's method used %i steps and the root_finding one used %i steps. Compare to the simple Newton's method from part A with %i steps.\n",step_rosen_quasi, step_rosen_root, step_rosen_A);
	printf("The analytical minimum is at x=(1,1). Quasi Newton found x=(%g,%g) and root-finding found x=(%g,%g).\n",x_rosen_quasi->data[0], x_rosen_quasi->data[1], x_rosen_root->data[0],x_rosen_root->data[1]);


	/*Himmelblau's function*/
	printf ("\nThe second test is Himmelblau's function.\n");
	gsl_vector_set (x_himmel_quasi, 0, 2);
	gsl_vector_set (x_himmel_quasi, 1, 2);
	gsl_vector_set (x_himmel_root, 0, 2);
	gsl_vector_set (x_himmel_root, 1, 2);
	int step_himmel_quasi =  quasi_newton_w_H (&himmel_wo_H, x_himmel_quasi, 1e-6);
	gsl_vector_set (f_call, 2, 0);
	int step_himmel_root =  newton_w_jacobian (&himmelblau_with_J, x_himmel_root, 1e-6, f_call);
	printf("Quasi Newton's method used %i steps and root-finding used %i steps. Compare to Newton's method from Part A with %i steps used.\n",step_himmel_quasi, step_himmel_root, step_himmel_A);
	printf("The local minima are analytically found at x=(3.0,2.0), x=(-2.805118,3.131312), x=(-3.779310,-3.283186) and x=(3.584428,-1.848126).\n");
	printf("Quasi Newton converged at x=(%g,%g) and root-finding at x=(%g,%g).\n",x_himmel_quasi->data[0], x_himmel_quasi->data[1], x_himmel_root->data[0],x_himmel_root->data[1]);

	printf("\n\nClearly the newton's method from Part A converges faster/in fewer steps. That is not surprising though, as it uses more knowledge than the Quasi Newton in the form of the analytically supplied Hessian matrix. Root-finding optimizes in one direction at a time, so it is a bit slower than the Part A Newton's method. Worst is the Quasi Newton method, as it updates the Hessian matrix at every step. The step size can be chosen more appropriately to each task, but it will likely still converge slower. It is, however, more general as it does not require an analytical Hessian.\n");


	printf("Now to use minimization to solve the posed non-linear least-squares fitting problem.\n");
	printf("The provided data was hard-coded into the main.c file.\n");

	/*data*/
	double t[] = {0.23,1.29,2.35,3.41,4.47,5.53,6.59,7.65,8.71,9.77};
	double y[] = {4.64,3.38,3.01,2.55,2.29,1.67,1.59,1.69,1.38,1.46};
	double e[] = {0.42,0.37,0.34,0.31,0.29,0.27,0.26,0.25,0.24,0.24};
	int N = sizeof(t)/sizeof(t[0]);

	/*initial guess*/
	gsl_vector* x_fit = gsl_vector_alloc(3);
	gsl_vector_set(x_fit, 0, 5.0);
	gsl_vector_set(x_fit, 1, 2.0);
	gsl_vector_set(x_fit, 2, 1.0);
	printf("Initial guess is (A,T,B) = (%g,%g,%g).\n",x_fit->data[0],x_fit->data[1],x_fit->data[2]);
	int step_fit = quasi_newton_w_H(&function_fit_wo_H,x_fit,1e-6);
	printf("Quasi Newton's method converged in %i steps. The solution can be inspected in the plot later.\n",step_fit);

	/*writing data into data.txt*/
	for(int i=0;i<N;i++){
		fprintf(stderr,"%g %g %g\n",t[i],y[i],e[i]);
	}
	fprintf(stderr,"\n\n\n");
	/*producing fit data points and printing to data.txt*/
	double x_min = 0, x_max = 10;
	double step_x = 1.00/100.00;
	double data;

	double A = x_fit->data[0], T = x_fit->data[1], B = x_fit->data[2];
	for(double i = x_min; i<x_max; i+=step_x){
		data = A*exp(-i/T)+B;
		fprintf(stderr,"%g %g\n", i, data);
	}
	printf("The fitted data is printed to data.txt and plottet in the figure.\n");
	printf("This concludes part B of this exercise.\n\n\n");

	
	
	/*Part C*/
	printf("Part C initializing.\n");
	printf("The Downhill simplex was implemented based on the lecture notes.\n");
	printf("The Downhill simplex will be tested on the three previous systems and then compared to the methods of part A and B.\n");
	printf("Three random points between 0 and 10 will be the initial point for each simplex.\n");

	int dim = 2;
	int dim_fit = 3;

	double eps_C = 1e-6;

	double** simplex_rosen = (double**)calloc(dim+1,sizeof(double*));
	for(int column = 0; column<dim+1; column++) simplex_rosen[column] = (double*)calloc(dim,sizeof(double));
	double** simplex_himmel = (double**)calloc(dim+1,sizeof(double*));
	for(int column = 0; column<dim+1; column++) simplex_himmel[column] = (double*)calloc(dim,sizeof(double));
	double** simplex_f = (double**)calloc(dim_fit+1,sizeof(double*));
	for(int column = 0; column<dim_fit+1; column++) simplex_f[column] = (double*)calloc(dim_fit,sizeof(double));
	
	
	double rnd;

	/*Rosenbrock*/
	printf("\nThe first test is Rosenbrock's function.\n");
	for(int n=0; n<dim+1; n++){
		for(int i=0; i<dim; i++){
			rnd = (double)rand()/(double)RAND_MAX*10;
			simplex_rosen[n][i] = rnd;
		}
	}

	printf("The initial points were\n");
	for(int n=0; n<dim+1; n++){
		for(int i=0; i<dim; i++){
			printf(" %g",simplex_rosen[n][i]);
		}
	printf("\n");
	}
	
	printf("Running minimization.\n");
	downhill_simplex(rosenbrock_simple, simplex_rosen, dim, eps_C);
	printf("The analytical minimum is x=(1,1). The found minimal simplex is cornered at\n");
	for(int n=0; n<dim+1; n++){
		printf("[");
		for(int i=0; i<dim; i++){
			printf(" %g ",simplex_rosen[n][i]);
		}
		printf("] \n");
	}


	/*Himmelblau's function*/
	printf("\nNow for Himmelblau's function.\n");

	for(int n=0; n<dim+1; n++){
		for(int i=0; i<dim; i++){
			rnd = (double)rand()/RAND_MAX*10;
			simplex_himmel[n][i] = rnd;
		}
	}
	printf("The initial simplex is cornered at\n");

	for(int n=0; n<dim+1; n++){
                printf("[");
                for(int i=0; i<dim; i++){
                        printf(" %g ",simplex_himmel[n][i]);
                }
                printf("] \n");
        }

	printf("Commencing minimization.\n");
	downhill_simplex(himmel_simple, simplex_himmel, dim, eps_C);
	printf("The four analytical local minima in this region are x=(3.0,2.0), x=(−2.805118,3.131312), x=(−3.779310,−3.283186) and x=(3.584428,-1.848126).\n");
	printf("The minimal simplex has corners at\n");
	
	for (int n=0; n<dim+1; n++) {
		printf("[");
                for(int i=0; i<dim; i++){
                        printf(" %g ",simplex_himmel[n][i]);
                }
                printf("] \n");
        }


	/*Data fint*/
	printf("\nNow for minimixation of the non-linear least_squares fitting.\n");

	for(int n=0; n<dim_fit+1; n++){
		for(int i=0; i<dim_fit; i++){
			rnd = (double)rand()/RAND_MAX*10;
			simplex_f[n][i] = rnd;
		}
	}

	printf("The initialized simplex has corners at\n");
	for (int n=0; n<dim_fit+1; n++) {
		fprintf(stdout,"[");
		for (int i=0; i<dim_fit; i++){
			fprintf(stdout," %g ",simplex_f[n][i]);
		}
	fprintf(stdout,"] \n");
	}

	printf("Minimizing.\n");
	downhill_simplex(fit_simple,simplex_f,dim_fit,eps_C);
	printf("Quasi Newton found (A,T,B) = (%g,%g,%g).\n",x_fit->data[0],x_fit->data[1],x_fit->data[2]);
	printf("The minimal simplex has corners at\n");
	for(int n=0; n<dim_fit+1; n++){
		printf("[");
		for(int i=0; i<dim_fit; i++){
			printf(" %g ",simplex_f[n][i]);
		}
		printf("] \n");
	}

	printf("\n\nHaving implemented the downhill simplex method with equally as good results as the other methods, part C is done.\n");

	gsl_vector_free(x_rosen_A);
	gsl_vector_free(x_himmel_A);
	gsl_vector_free(x_rosen_quasi);
	gsl_vector_free(x_rosen_root);
	gsl_vector_free(x_himmel_quasi);
	gsl_vector_free(x_himmel_root);
	gsl_vector_free(x_fit);
	gsl_vector_free(f_call);
	free(simplex_rosen);
	free(simplex_himmel);
	free(simplex_f);
	return 0;
}
