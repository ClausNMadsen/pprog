#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_errno.h>
#include"qr_gs.h"

int newton_w_jacobian(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J, gsl_vector* f_call), gsl_vector* x, double eps, gsl_vector* f_call){

	int n_eq = x->size;
	gsl_matrix* R = gsl_matrix_alloc(n_eq,n_eq);
	gsl_matrix* J = gsl_matrix_alloc(n_eq,n_eq);
	gsl_vector* fx= gsl_vector_alloc(n_eq);

	gsl_vector* dx = gsl_vector_alloc(n_eq);
	gsl_vector* new_fx = gsl_vector_alloc(n_eq);
	gsl_vector* cfx = gsl_vector_alloc(n_eq);
	double lambda = 1.00;
	int step=0;
	double f_norm, new_f_norm;

	do{
		step++;
		f(x,fx,J,f_call); /*update J and fx*/
		gsl_vector_memcpy(cfx,fx); /*solve Jdx=-fx*/

		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,cfx,dx);
		lambda = 1.00;
		gsl_vector_scale(dx, -lambda); /*new x is x + lambda*(-dx)*/
		gsl_vector_add(x,dx);

		f(x,new_fx,J,f_call);

		f_norm = gsl_blas_dnrm2(fx);
		new_f_norm = gsl_blas_dnrm2(new_fx);

		while(new_f_norm > (1-lambda/2.00)*f_norm && lambda > 1.00/64.00){/*rescaling lambda to make more intelligent steps*/
			lambda /= 2.00;
			gsl_vector_scale(dx,lambda);
			f(x,new_fx,J,f_call);
			new_f_norm = gsl_blas_dnrm2(new_fx);
		}

		f(x,new_fx,J,f_call);
		new_f_norm = gsl_blas_dnrm2(new_fx);
	}while(new_f_norm > eps);

	gsl_matrix_free(R);
	gsl_matrix_free(J);
	gsl_vector_free(fx);
	gsl_vector_free(dx);
	gsl_vector_free(cfx);
	gsl_vector_free(new_fx);
	return step;
}
