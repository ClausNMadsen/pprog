#ifndef HAVE_JACOBI_H
#define HAVE_JACOBI_H

/*full sweeper Jacobi diagonalization*/
int jacobi_diag(gsl_matrix* A, gsl_vector* e, gsl_matrix* V);

/*row by row sweeper Jacobi diagonalization*/
int jacobi_one_by_one( gsl_matrix* A,  gsl_vector* e,  gsl_matrix* V,  int n_eig_vals);

#endif
