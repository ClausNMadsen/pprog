#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include"jacobi.h"
void sym_rnd_matrix_initialize(gsl_matrix* A, int size);

int main(int argc, char** argv){
        int size = atoi(argv[1]);
        gsl_matrix* A = gsl_matrix_alloc(size,size);
        gsl_matrix* V = gsl_matrix_alloc(size,size);
        gsl_vector* e = gsl_vector_alloc(size);

        sym_rnd_matrix_initialize(A,size);

        int sweeps;
        sweeps = jacobi_diag(A,e,V);
        printf("%d\n",sweeps);
	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);
        return 0;
}
