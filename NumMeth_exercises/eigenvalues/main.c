#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_blas.h>
#include"jacobi.h"

/*simple matrix print function*/
void matrix_print(const char* s, gsl_matrix* A){
        printf("%s\n",s);
        for(int i=0; i<A->size1; ++i){
                for(int j=0; j<A->size2; ++j){
                        printf("%10.3f",gsl_matrix_get(A,i,j));
                }
                printf("\n");
        }
}

	
int main(int argc, char** argv){
	/*randomly varied symmetric matrix of size sizexsize*/

/*--------------------Part A-------------*/
	if(argc != 2){printf("error: main takes only a single input\n");return-1;}
	
	int size = atoi(argv[1]);
	gsl_matrix* A = gsl_matrix_alloc(size,size);

	for(int i=0; i<A->size1; ++i){
		gsl_matrix_set(A,i,i,(double)rand()/RAND_MAX);
		for(int j=0; j<i; j++){
			gsl_matrix_set(A,i,j,(double)rand()/RAND_MAX);
			gsl_matrix_set(A,j,i,gsl_matrix_get(A,i,j));
		}
	}

	/*run Jacobi diagonalization on A*/
	gsl_vector* e = gsl_vector_alloc(A->size1);
	gsl_matrix* V = gsl_matrix_alloc(A->size1,A->size2);

	int sweeps = jacobi_diag(A,e,V);
	fprintf(stderr,"n=%d, sweeps=%i\n",size,sweeps);
	
	if(size<10){
		printf("The original random, symmetric matrix A is:\n");
		for(int i=0; i<A->size1; ++i){
			for(int j=0; j<=i; ++j){
				printf("%10.3g",gsl_matrix_get(A,i,j));
			}
			for(int j=i+1; j<A->size2; ++j){
				printf("%10.3g",gsl_matrix_get(A,j,i));
			}
			printf("\n");
		}
		/*re-constructing original A to emply BLAS-routines*/ 
		gsl_matrix* A_org = gsl_matrix_alloc(A->size1,A->size2);
		for(int i = 0; i<A->size1; ++i){
			for(int j = 0; j<i; ++j){
				gsl_matrix_set(A_org,i,j,gsl_matrix_get(A,i,j));
			}
			for(int j = i; j<A->size2; ++j){
				gsl_matrix_set(A_org,i,j,gsl_matrix_get(A,j,i));
			}
		}

		/* temporary intermediate matrix*/
		gsl_matrix* A_prime = gsl_matrix_alloc(A->size1,A->size2);	
		gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,A_org,V,0.0,A_prime);
		gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A_prime,0.0,A_org);
		printf("VTAV should be diagonal with the eigenvalues in the vector e\n");
		matrix_print("VTAV=",A_org);

		printf("The vector e is\ne=\n");
		for(int i=0; i<e->size; ++i){
			printf("%.3g\n",e->data[i]);
		}
		gsl_matrix_free(A_org);
		gsl_matrix_free(A_prime);
	}


	gsl_matrix_free(A);
	gsl_matrix_free(V);
	gsl_vector_free(e);


return 0;
}
			
