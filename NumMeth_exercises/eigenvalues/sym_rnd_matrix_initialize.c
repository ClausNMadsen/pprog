#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>

void sym_rnd_matrix_initialize(gsl_matrix* A, int size){
	  for(int i = 0; i<A->size1; ++i){
                gsl_matrix_set(A,i,i,(double)rand()/RAND_MAX);
                for(int j = 0; j<i; ++j){
                        double rnd = (double)rand()/RAND_MAX;
                        gsl_matrix_set(A,i,j,rnd);
                        gsl_matrix_set(A,j,i,rnd);
                }
        }
}
