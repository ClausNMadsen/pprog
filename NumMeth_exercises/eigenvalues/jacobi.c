#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include"jacobi.h"

int jacobi_diag(gsl_matrix* A, gsl_vector* e, gsl_matrix* V){
	/*this code is straight up stolen from Dmi's example*/
	/* Jacobi diagonalization, destroys upper triangle of A, eigenvalues/-vectors are stored in e and V*/
	int changed, sweeps=0, n=A->size1;
	for(int i=0; i<n; ++i){
		gsl_vector_set(e,i,gsl_matrix_get(A,i,i));
	}
	gsl_matrix_set_identity(V);
	do{changed =0; sweeps++; int p,q;
		for(p=0; p<n;++p)for(q=p+1;q<n;++q){
			double app=gsl_vector_get(e,p);
			double aqq=gsl_vector_get(e,q);
			double apq=gsl_matrix_get(A,p,q);
			double phi=0.5*atan2(2*apq,aqq-app);
			double c = cos(phi), s = sin(phi);
			double app1=c*c*app-2*s*c*apq+s*s*aqq;
			double aqq1=s*s*app+2*s*c*apq+c*c*aqq;
			if(app1!=app || aqq1!=aqq){ changed=1;
				gsl_vector_set(e,p,app1);
				gsl_vector_set(e,q,aqq1);
				gsl_matrix_set(A,p,q,0.0);
				for(int i=0;i<p;i++){
					double aip=gsl_matrix_get(A,i,p);
					double aiq=gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,i,p,c*aip-s*aiq);
					gsl_matrix_set(A,i,q,c*aiq+s*aip); }
				for(int i=p+1;i<q;i++){
					double api=gsl_matrix_get(A,p,i);
					double aiq=gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,p,i,c*api-s*aiq);
					gsl_matrix_set(A,i,q,c*aiq+s*api); }
				for(int i=q+1;i<n;i++){
					double api=gsl_matrix_get(A,p,i);
					double aqi=gsl_matrix_get(A,q,i);
					gsl_matrix_set(A,p,i,c*api-s*aqi);
					gsl_matrix_set(A,q,i,c*aqi+s*api); }
				for(int i=0;i<n;i++){
					double vip=gsl_matrix_get(V,i,p);
					double viq=gsl_matrix_get(V,i,q);
					gsl_matrix_set(V,i,p,c*vip-s*viq);
					gsl_matrix_set(V,i,q,c*viq+s*vip); }
				} } }while(changed!=0);
return sweeps; }


/*the eigenvalue by eigenvalue modified version of Dmi's example*/
int jacobi_one_by_one( gsl_matrix* A,  gsl_vector* e,  gsl_matrix* V,  int n_eig_vals){
	int changed, sweeps = 0, n=A->size1; /*initialized values*/
	for(int i=0; i<n; ++i) gsl_vector_set(e,i,gsl_matrix_get(A,i,i)); /*Copy diagonal of A to e*/
	gsl_matrix_set_identity(V);
	do{
		changed=0;
		++sweeps;
		int p,q;
		for(p=0; p<n_eig_vals; ++p)for(q=p+1;q<n;q++){
			double app=gsl_vector_get(e,p);
			double aqq=gsl_vector_get(e,q);
			double apq=gsl_matrix_get(A,p,q); /*A(p,q)=A(q,p) since A is diagonal*/
			double phi=0.5*atan2(2*apq,aqq-app); /*angle minimizing off diagnoal element*/
			double c = cos(phi), s = sin(phi); /*Given's rotation transformation*/
			double app1 = c*c*app - 2*s*c*apq + s*s*aqq; /*On eigenvalues*/
			double aqq1 = s*s*app + 2*s*c*apq + c*c*aqq; 
			if(app1!= app || aqq1!=aqq){ changed=1; /*If no change in eigenvalue, algorithm has converged/can't do better*/
				gsl_vector_set(e,p,app1);
				gsl_vector_set(e,q,aqq1);
				gsl_matrix_set(A,p,q,0.0); /*eliminate upper off-diagonal element*/
				for(int i=0; i<p; ++i){ /*Transform each row*/
					double aip=gsl_matrix_get(A,i,p);
					double aiq=gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,i,p,c*aip - s*aiq);
					gsl_matrix_set(A,i,q,c*aiq + s*aip); }
				for(int i=p+1;i<q;i++){ /*Transform each row between q and p*/
					double api=gsl_matrix_get(A,p,i);
					double aiq=gsl_matrix_get(A,i,q);
					gsl_matrix_set(A,p,i,c*api-s*aiq);
					gsl_matrix_set(A,i,q,c*aiq+s*api); }
				for(int i=q+1;i<n;i++){ /*transform last columns*/
					double api=gsl_matrix_get(A,p,i);
					double aqi=gsl_matrix_get(A,q,i);
					gsl_matrix_set(A,p,i,c*api-s*aqi);
					gsl_matrix_set(A,q,i,c*aqi+s*api); }
				for(int i=0;i<n;i++){ /*repeat actions on identity to get eigenvectors*/
					double vip=gsl_matrix_get(V,i,p);
					double viq=gsl_matrix_get(V,i,q);
					gsl_matrix_set(V,i,p,c*vip-s*viq);
					gsl_matrix_set(V,i,q,c*viq+s*vip); }
			}
	 	}
	} while(changed!=0); /*if nothing changed, stop*/

	for (int ii = n_eig_vals; ii < V->size1; ii++) {
		gsl_vector_set(e,ii,0);
  		for(int jj = 0; jj < V->size2; jj++) {
    			gsl_matrix_set(V,jj,ii,0);
  		}
	}
return sweeps;
}
