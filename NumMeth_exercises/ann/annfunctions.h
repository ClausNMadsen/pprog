#ifndef HAVE_ANN_H
#define HAVE_ANN_H
typedef struct {int N; double (*f)(double); gsl_vector* params;} ann;
ann* ann_alloc(int N, double (*f)(double));
void ann_free(ann* neural_network);
double ann_feed_forward(ann* neural_network, double x);
double ann_feed_forward_deriv(ann* neural_network, double x);
double ann_feed_forward_antideriv(ann* neural_network, double lower, double x);
void ann_train(ann* neural_network, gsl_vector* x_list, gsl_vector* y_list);
double f(double x);
double f_deriv(double x);
#endif
