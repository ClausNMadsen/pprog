#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_multimin.h>
#include<gsl/gsl_errno.h>
#include"annfunctions.h"
#include"integrators.h"

ann* ann_alloc(int N, double(*f)(double)){
	/*N is number of neurons, f is activation function*/
	ann* neural_network = malloc(sizeof(ann));
	neural_network->N = N;
	neural_network->params = gsl_vector_alloc(3*N);
	neural_network->f = f;
	return neural_network;
}

void ann_free(ann* neural_network){
	gsl_vector_free(neural_network->params);
	free(neural_network);
}

double ann_feed_forward(ann* neural_network, double x){
	double y = 0, a_i, b_i, w_i, y_i;
	for(int i=0; i<neural_network->N; ++i){
		a_i = neural_network->params->data[i];
		b_i = neural_network->params->data[neural_network->N+i];
		w_i = neural_network->params->data[neural_network->N*2+i];
		y_i = neural_network->f((x-a_i)/b_i) * w_i/b_i;
		y += y_i;
	}
	return y;
}

double ann_feed_forward_deriv(ann* neural_network, double x){
	double y = 0, a_i, b_i, w_i, fx, fdxx, dx=1e-3;
	for(int i = 0; i<neural_network->N; ++i){
		a_i = neural_network->params->data[i];
		b_i = neural_network->params->data[neural_network->N+i];
		w_i = neural_network->params->data[neural_network->N*2+i];
		fx  = neural_network->f((x-a_i)/b_i) * w_i/b_i;
		fdxx = neural_network->f((x+dx-a_i)/b_i)*w_i/b_i;
		y += (fdxx-fx)/dx;
	}
	return y;
}

double ann_feed_forward_antideriv(ann* neural_network, double lower, double x){
	/*This is an integral in the gaussian basis on x*/
	double y=0, a_i, b_i, w_i, err;
	for(int i = 0; i<neural_network->N; ++i){
                a_i = neural_network->params->data[i];
                b_i = neural_network->params->data[neural_network->N+i];
                w_i = neural_network->params->data[neural_network->N*2+i];
		y += w_i*adapt(neural_network->f, (lower -a_i)/b_i, (x - a_i)/b_i, 1e-3, 1e-3, &err);
	}
	return y;
}

void ann_train(ann* neural_network, gsl_vector* x_list, gsl_vector* y_list){
	double mismatch(const gsl_vector* parameters, void* params){
		gsl_vector_memcpy(neural_network->params,parameters);
		double delta = 0;
		for(int i = 0; i < x_list->size; ++i){
			double x = x_list->data[i];
			double y = y_list->data[i];
			double ff = ann_feed_forward(neural_network,x);
			delta += (ff-y)*(ff-y);
		}
		return delta/x_list->size;
	}

	gsl_vector* parameters = gsl_vector_alloc(neural_network->params->size);
	gsl_vector_memcpy(parameters,neural_network->params);

	gsl_multimin_function F;
	F.f=mismatch;
	F.n = neural_network->params->size;
	F.params = NULL;

	gsl_vector* step = gsl_vector_alloc(F.n);
	gsl_vector_set_all(step,0.5);

	gsl_multimin_fminimizer* state = gsl_multimin_fminimizer_alloc(gsl_multimin_fminimizer_nmsimplex2,F.n);
	gsl_multimin_fminimizer_set(state, &F, parameters, step);

	int iter=0, status;
	double acc=0.01;
	do{
		++iter;
		int iteration_status = gsl_multimin_fminimizer_iterate(state);
		if(iteration_status != 0){
			fprintf(stderr,"Improvement not possible.\n");
			break;
		}
		status = gsl_multimin_test_size(state->size, acc);
	}while(status ==GSL_CONTINUE && iter < 10000);

	gsl_vector_memcpy(neural_network->params,state->x);

	gsl_vector_free(step);
	gsl_multimin_fminimizer_free(state);
	gsl_vector_free(parameters);
}
