#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_matrix.h>
#include"annfunctions.h"
	
double activation_function(double x){
	return exp(-x*x);
}
double function_to_fit(double x){
	return 1/(1.0+exp(-2*(x-5)));
}
double function_to_fit_deriv(double x){
	return 2.0*exp(-2.0*(x-5.0))/((exp(-2.0*(x-5.0))+1.0)*(exp(-2.0*(x-5.0))+1.0));
}
double function_to_fit_antiderive(double x){
	return 1.0/2.0*(log(exp(2.0*x)+exp(10))-log(1+exp(10)));
}


int main(void){
	/*Part A*/
	printf("Exercise A initiating.\n\n");
	printf("A simple artificial neural network with user provided activation functions has been implemented.\n");
	printf("y = 1/(1+exp(-2*(x-5))) was chosen, as I find it an aesthetically pleasing function. It is also a function with almost linear to quite curving parts.\n");

	int N = 3;
	int N2 = 7;
	ann* neural_network = ann_alloc(N,activation_function);
	ann* neural_network2 = ann_alloc(N2,activation_function);
	printf("Two networks are trained, one with %i hidden neurons and another with %i hidden networks.\n",neural_network->N, neural_network2->N);

	int training_points = 600;
	gsl_vector* x_list = gsl_vector_alloc(training_points);
	gsl_vector* y_list = gsl_vector_alloc(training_points);
	double int_start = 0, int_stop = 10, fx_fit, x;
	double delta_int = (int_stop - int_start)/((double)training_points-1.00);

	for(int i = 0; i<training_points; ++i){
		x = int_start + i*delta_int;
		fx_fit = function_to_fit(x);
		gsl_vector_set(x_list,i,x);
		gsl_vector_set(y_list,i,fx_fit);
	}

	/*initiating guesses*/
	for(int i=0; i < N; ++i){
		gsl_vector_set(neural_network->params,i,2.0);
		gsl_vector_set(neural_network->params,N + i,1.0);
		gsl_vector_set(neural_network->params,2*N + i,2.0/(double)N);
	}
	for(int i=0; i < N2; ++i){
		gsl_vector_set(neural_network2->params,i,9.0);
		gsl_vector_set(neural_network2->params,N2 + i,1.0);
		gsl_vector_set(neural_network2->params,2*N2 + i,2.0/(double)N2);
	}

	printf("The networks are trained using %i training points of the analytical function.\n",training_points);

	ann_train(neural_network,x_list,y_list);
	ann_train(neural_network2,x_list,y_list);
	printf("Training done.\n");

	/*generating plot*/
	int N_plot = 1000;
	double plot_start = 0.0, plot_stop = 10.0, x_plot, y_plot, y_plot2, y_theory;
	double delta_plot = (plot_stop - plot_start)/((double)N_plot-1.00);

	for(int i=0; i<N_plot; ++i){
		x_plot = plot_start + i* delta_plot;
		y_plot = ann_feed_forward(neural_network,x_plot);
		y_plot2 = ann_feed_forward(neural_network2,x_plot);
		y_theory = function_to_fit(x_plot);
		fprintf(stderr,"%g %g %g %g\n",x_plot, y_plot, y_plot2, y_theory);
	}

	printf("A plot of the resulting fits using the %i and %i hidden neuron neural networks to the function mentioned earlier is shown in plot_A.svg.\n",N,N2);

	printf("\nIt is clear in the plot that the %i hidden neuron neural network is better at fitting the Gaussian than the %i hidden neuron neural network. This should be clear from the fact that the more neurons you have to work with, the more parameters to adjust and thus, ideally at least, you should get a better fit.\n",N>N2 ? N:N2, N<N2 ? N:N2);

	printf("\nWith the successful fitting, part A is done.\n");
	

	/*Part B*/
	printf("\n\nPart B initiating.\n");
	printf("\nDerivative and anti-derivative feed-forward function implemented.\n");
	printf("These two functions are implemented to use the Euler-step of the user provided activation function for the derivative feed-forward and the integrated activation function, using the adaptive integrator from the integration exercise, for the anti-derivative feed-forward.\n");
	printf("Using the already trained neurons from part A, %i hidden neurons using %i training points, the neurons are used to fit the derivative and anti-derivative of the chosen logistic function.\n",N2,training_points);


	double y_plot2_deriv, y_deriv_theory, y_plot2_antideriv, y_antideriv_theory;
	fprintf(stderr,"\n\n\n");
	for(int i=0; i<N_plot; ++i){
		x_plot = plot_start + i * delta_plot;
		y_plot2_deriv = ann_feed_forward_deriv(neural_network2,x_plot);
		y_deriv_theory = function_to_fit_deriv(x_plot);
		y_plot2_antideriv = ann_feed_forward_antideriv(neural_network2,0.0,x_plot);
		y_antideriv_theory = function_to_fit_antiderive(x_plot);
		fprintf(stderr,"%g %g %g %g %g\n",x_plot,y_plot2_deriv,y_deriv_theory,y_plot2_antideriv,y_antideriv_theory);
	}
	printf("Fitting done.\n");
	printf("\nThe resulting fits can be seen in the figures plot_deriv.svg and plot_antiderive.svg.\n");
	printf("Having implemented the derivative and anti-derivative approximations using the already trained neurons, part B is concluded.\n");


	return 0;
}
