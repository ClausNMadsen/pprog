#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"root_finding.h"

int main(void){

	/*Part A*/
	printf("Exercise A executing.\n\n");
	
	gsl_vector* f_call = gsl_vector_alloc(3);
	gsl_vector* x_lineq = gsl_vector_alloc(2);
	gsl_vector* x_rosen = gsl_vector_alloc(2);
	gsl_vector* x_himmel = gsl_vector_alloc(2);


	/*The system of linear equations*/
	gsl_vector_set(x_lineq,0,0);
	gsl_vector_set(x_lineq,1,1);
	
	printf("Newton's method with provided analytical Jacobian and back-tracking linesearch is implemented.\n\n");
	printf("The first test is solving the system of equations:\nA*x*y = 1\nexp(-x) + exp(-y) = 1 + 1/A, where A=10000.\n");

	printf("The initial point was chosen to be x0 = (%1.1g,%1.1g).\n",x_lineq->data[0],x_lineq->data[1]);

	gsl_vector_set(f_call,0,0);
	int steplineq_A = newton_w_jacobian(&system_of_equations_with_J, x_lineq, 1e-6,f_call);
	printf("Newton's method used %i steps and %g function calls.\n",steplineq_A,f_call->data[0]);
	
	printf("The symmetric solutions are analytically found at x=[9.1061,0.00010981] or [0.00010981,9.1061].\n");
	printf("Newton's method found the minimum x=(%g,%g)\n\n",x_lineq->data[0],x_lineq->data[1]);


	/*Rosenbrock's valley function*/
	printf("The second test is Rosenbrock's valley function.\n");
	gsl_vector_set(x_rosen,0,0);
	gsl_vector_set(x_rosen,1,0);
	printf("Initial point chosen to be x0=(%g,%g).\n",x_rosen->data[0],x_rosen->data[1]);
	gsl_vector_set(f_call,1,0);
	int step_rosen_A = newton_w_jacobian(&rosenbrock_valley_with_J,x_rosen, 1e-6,f_call);
	printf("Newton's method used %i steps and %g function calls.\n",step_rosen_A,f_call->data[1]);
	printf("The root should be at x=[1,1] and Newton's method found x=(%g,%g).\n\n",x_rosen->data[0],x_rosen->data[1]);

	
	/*Himmelblau's function*/
	printf("The third test is Himmelblau's function.\n");
	gsl_vector_set(x_himmel, 0, 1);
	gsl_vector_set(x_himmel, 1, 1);
	printf("Initial point chosen to be x0=(%g,%g).\n",x_himmel->data[0],x_himmel->data[1]);
	gsl_vector_set(f_call,2,0);
	int step_himmel_A = newton_w_jacobian(&himmelblau_with_J, x_himmel, 1e-6, f_call);
	printf("Newton's method used %i steps and %g function calls.\n", step_himmel_A,f_call->data[2]);
	printf("Analytical solutions are found at four local minima: at x=(3.0,2.0), x=(-2.805118,3.131312), x=(-3.779310,-3.283186) and (3.584428,-1848126).\n");
	printf("Newton's method found x=(%g,%g).\n",x_himmel->data[0],x_himmel->data[1]);
	printf("Thus, part A is done.\n\n\n");


	/*Part B*/
	printf("\nExercise B initializing.\n\n");
	printf("Newton's method with numerical calculation of the Jacobian and back-tracking linesearch is implemented.\n");
	
	gsl_vector* f_call_B = gsl_vector_alloc(4);
	gsl_vector* x_lineq_B = gsl_vector_alloc(2);
	gsl_vector* x_rosen_B = gsl_vector_alloc(2);
	gsl_vector* x_himmel_B = gsl_vector_alloc(2);
	double stepsize_B = 1e-3;
	double eps_B = 1e-6;

	printf("The implementation is compared to Newton's method with provided analytical Jacobian from part A.\n");

	/*Linear equations, part b*/
	gsl_vector_set(x_lineq_B,0,0);
	gsl_vector_set(x_lineq_B,1,1);
	gsl_vector_set(f_call_B,0,0);
	printf("\nFirst the system of equations.\nThe same initial point, (%g,%g), is used.\n",x_lineq_B->data[0],x_lineq_B->data[1]);
	int step_lineq_B = newton(&function_linear_equation,x_lineq_B,stepsize_B,eps_B,f_call_B);
	printf("Newton's method with numerical Jacobian used %i steps and %g function calls.\n",step_lineq_B,f_call_B->data[0]);
	printf("Compare with %i steps and %g function calls for the implementation with user provided Jacobian and same initial point.\n",steplineq_A,f_call->data[0]);
	printf("Analytically, the symmetric solutions are x=(9.1061,0.00010981) or x=(0.00010981,9.1061).\n");
	printf("Newton's method with numerical Jacobian found x=(%g,%g).\n",x_lineq_B->data[0],x_lineq_B->data[1]);

	
	/*Rosenbrock's valley function*/
	gsl_vector_set(x_rosen_B,0,0);
	gsl_vector_set(x_rosen_B,1,0);
	gsl_vector_set(f_call_B,1,0);
	printf("\nThe second test is again Rosenbrock's valley function.\n");
	printf("Initial point is again x=(%g,%g).\n",x_rosen_B->data[0],x_rosen_B->data[1]);
	int step_rosen_B = newton(&rosenbrock, x_rosen_B, stepsize_B,eps_B,f_call_B);
	printf("Newton's method with numerical Jacobian used %i steps and %g function calls.\n",step_rosen_B,f_call_B->data[1]);
	printf("Compare with %i steps and %g function calls for the implementation with user provided Jacobian and same initial point.\n",step_rosen_A,f_call->data[1]);
	printf("The analytical root is x=(1,1). Newton's method with numerical Jacobian found x=(%g,%g).\n",x_rosen_B->data[0],x_rosen_B->data[1]);

	
	/*Himmelblau's function*/
	gsl_vector_set(x_himmel_B,0,1);
	gsl_vector_set(x_himmel_B,1,1);
	gsl_vector_set(f_call_B,2,0);
	printf("\nThird test is again Himmelblau's function.\n");
	printf("Initial point is again x=(%g,%g).\n",x_himmel_B->data[0],x_himmel_B->data[1]);
	int step_himmel_B = newton(&himmel,x_himmel_B,stepsize_B,eps_B,f_call_B);
	printf("Newton's method with numerical Jacobian used %i steps and %g function calls.\n",step_himmel_B,f_call_B->data[2]);
	printf("Compare with %i steps and %g function calls for the implementation with user provided Jacobian and same initial point.\n",step_himmel_A,f_call->data[2]);
	printf("Analytical solution at four local minima, x=(3.0,2.0), x=(-2.805118,3.131312), x=(-3.779310,-3.283186) and x=(3.584428,-1.848126).\n");
	printf("Newton's method with numerical Jacobian found x=(%g,%g).\n",x_himmel_B->data[0],x_himmel_B->data[1]);

	printf("\n\nOne can conclude that in general, the analytical solver is likely most often the most efficient solver, using both less steps and function calls. One might get lucky and choose great starting point and stepsize parameters making the numerical solver faster or more efficient.\n");

		



	/*GSL subsection of part B*/

	printf("\n\nNow to compare with the multiroots GSL functions. Those are implemented now, with the initial point.\n");

	/*GSL on system of equations*/
	double x_lineq_gsl = 0;
	double y_lineq_gsl = 1;
	double *px_lineq = &x_lineq_gsl;
	double *py_lineq = &y_lineq_gsl;
	gsl_root_lineq(px_lineq,py_lineq);

	/*GSL on Rosenbrock's valley function*/
	double x_rosen_gsl = 0;
	double y_rosen_gsl = 0;
        double *px_rosen = &x_rosen_gsl;
	double *py_rosen = &y_rosen_gsl;
	gsl_root_rosen(px_rosen,py_rosen);

	/*GSL on Himmelblau's function*/
	double x_himmel_gsl = 1;
	double y_himmel_gsl = 1;
	double *px_himmel = &x_himmel_gsl;
	double *py_himmel = &y_himmel_gsl;
	gsl_root_himmel(px_himmel,py_himmel);

	printf("\nIt seems in general these GSL library functions take fewer iterations than my own. The GSL functions are much more optimized than my rudimentary functions. Performance could likely be further improved by choosing another gsl_multiroot_fsolver type than hybrids.\n");


	/*PART C*/
	printf("\n\nPart C is now starting.\n");
	printf("Newton's method with user provided Jacobian and quadratic interpolation updated back-tracking linesearch is implemented and compared to the linear linesearch method from part A.\n");
	printf("As before, the implementation is tested on the provided system of equations, Rosenbrock's valley function and Himmelblau's function, with the same initial points as in both parts A and B.\n");
	gsl_vector* x_lineq_C = gsl_vector_alloc(2);
	gsl_vector* x_rosen_C = gsl_vector_alloc(2);
	gsl_vector* x_himmel_C = gsl_vector_alloc(2);
	gsl_vector* f_call_C = gsl_vector_alloc(3);

	/*System of equations*/
	printf("\nFirst the system of equations.\n");
	gsl_vector_set(x_lineq_C, 0, 0);
	gsl_vector_set(x_lineq_C, 1, 1);
	gsl_vector_set(f_call_C, 0, 0);
	int step_lineq_C = newton_w_jacobian_quad_int(&system_of_equations_with_J, x_lineq_C,1e-6,f_call_C);
	printf("The quadratic method used %i steps and %g function calls. The linear one used %i steps and %g calls.\n", step_lineq_C, f_call_C->data[0],steplineq_A, f_call->data[0]);
	printf("The quadratic method found the root x=(%g,%g).\n",x_lineq_C->data[0],x_lineq_C->data[1]);

	/*The Rosenbrock's valley function*/
	printf("\nSecond Rosenbrock's valley function.\n");
	gsl_vector_set(x_rosen_C, 0, 0);
	gsl_vector_set(x_rosen_C, 1, 0);
	gsl_vector_set(f_call_C, 1, 0);
	int step_rosen_C = newton_w_jacobian_quad_int(&rosenbrock_valley_with_J,x_rosen_C, 1e-6, f_call_C);
	printf("The quadratic method used %i steps and %g function calls. The linear one used %i steps and %g calls.\n",step_rosen_C,f_call_C->data[1],step_rosen_A,f_call->data[1]);
	printf("The quadratic method found the root x=(%g,%g).\n",x_rosen_C->data[0],x_rosen_C->data[1]);


	/*Himmelblau's function*/
	printf("\nThird Himmelblau's function.\n");
	gsl_vector_set(x_himmel_C, 0, 1);
	gsl_vector_set(x_himmel_C, 1, 1);
	gsl_vector_set(f_call_C, 2, 0);
	int step_himmel_C = newton_w_jacobian_quad_int(&himmelblau_with_J,x_himmel_C, 1e-6, f_call_C);
	printf("The quadratic method used %i steps and %g function calls. The linear one used %i steps and %g calls.\n",step_himmel_C,f_call_C->data[2],step_himmel_A,f_call->data[2]);
	printf("The quadratic method found the root x=(%g,%g).\n",x_himmel_C->data[0],x_himmel_C->data[1]);


	printf("\nIn general, the quadratic method is an improvement on the linear method. The Rosenbrock's example here suggests otherwise, but that discrepancy is caused by a very fortuitous choice of initial parameters in the linear case, making the problem one-dimensional.\n");
       printf("\nThis concludes part C of the exercise.\n");	
	gsl_vector_free(x_lineq);
	gsl_vector_free(x_rosen);
	gsl_vector_free(x_himmel);
	gsl_vector_free(f_call);
	gsl_vector_free(x_lineq_B);
	gsl_vector_free(x_rosen_B);
	gsl_vector_free(x_himmel_B);
	gsl_vector_free(f_call_B);
	gsl_vector_free(x_lineq_C);
	gsl_vector_free(x_rosen_C);
	gsl_vector_free(x_himmel_C);
	gsl_vector_free(f_call_C);

	return 0;
}
