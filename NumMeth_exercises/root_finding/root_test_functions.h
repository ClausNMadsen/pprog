#ifndef HAVE_ROOT_TEST_FUNCTIONS_H
#define HAVE_ROOT_TEST_FUNCTIONS_H
void system_of_equations_with_J(gsl_vector* x, gsl_vector* y, gsl_matrix* J, gsl_vector* f_call);
void rosenbrock_valley_with_J(gsl_vector* x, gsl_vector* y, gsl_matrix* J, gsl_vector* f_call);
void himmelblau_with_J(gsl_vector* x, gsl_vector* y, gsl_matrix* J, gsl_vector* f_call);
void function_linear_equation(gsl_vector* x, gsl_vector* y, gsl_vector* f_call);
void rosenbrock(gsl_vector* x, gsl_vector* y, gsl_vector* f_call);
void himmel(gsl_vector* x, gsl_vector* y, gsl_vector* f_call);
#endif
