#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_errno.h>

void system_of_equations_with_J(gsl_vector* x, gsl_vector* y, gsl_matrix* J, gsl_vector* f_call){
	gsl_vector_set(f_call, 0, f_call->data[0]+1 );
	double A=10000, x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, A*x_1*x_2 -1);
	gsl_vector_set(y,1, exp(-x_1) + exp(-x_2) - 1 - 1.0/A);

	double J_11 = A*x_2, J_12 = A*x_1, J_21 = -exp(-x_1), J_22=-exp(-x_2);
	gsl_matrix_set(J,0,0,J_11);
	gsl_matrix_set(J,0,1,J_12);
	gsl_matrix_set(J,1,0,J_21);
	gsl_matrix_set(J,1,1,J_22);
}

void rosenbrock_valley_with_J(gsl_vector* x, gsl_vector* y, gsl_matrix* J, gsl_vector* f_call){
	gsl_vector_set(f_call,1,f_call->data[1]+1);
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 2*(1-x_1)*(-1)+100*2*(x_2-x_1*x_1)*(-1)*2*x_1);
	gsl_vector_set(y,1, 100*2*(x_2-x_1*x_1));
	double J_11 = 2 - 400*(x_2) + 1200*x_1*x_1;
	double J_12 = -400*x_1, J_21 = -400*x_1, J_22 = 200;
	gsl_matrix_set(J,0,0,J_11);
	gsl_matrix_set(J,0,1,J_12);
	gsl_matrix_set(J,1,0,J_21);
	gsl_matrix_set(J,1,1,J_22);
}

void himmelblau_with_J(gsl_vector* x, gsl_vector* y, gsl_matrix* J, gsl_vector* f_call){
	gsl_vector_set(f_call,2,f_call->data[2]+1);
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 4*(x_1*x_1 + x_2 - 11)*x_1 + 2*(x_1+x_2*x_2 - 7));
	gsl_vector_set(y,1, 2*(x_1*x_1 + x_2 -11) + 4*(x_1 + x_2*x_2 -7)*x_2);

	double J_11 = 12*x_1*x_1 + 4*x_2 - 42;
	double J_12 = 4*(x_1+x_2), J_21 = 4*(x_1+x_2), J_22 = 4*x_1+12*x_2*x_2 -26;
	gsl_matrix_set(J,0,0,J_11);
	gsl_matrix_set(J,0,1,J_12);
	gsl_matrix_set(J,1,0,J_21);
	gsl_matrix_set(J,1,1,J_22);
}

void function_linear_equation(gsl_vector* x, gsl_vector* y, gsl_vector* f_call){
	gsl_vector_set(f_call,0,f_call->data[0]+1);
	double x_1 = x->data[0], x_2 = x->data[1];
	double A = 10000;
	gsl_vector_set(y,0, A*x_1*x_2-1);
	gsl_vector_set(y,1, exp(-x_1) + exp(-x_2) -1 - 1.0/A);
}

void rosenbrock(gsl_vector* x, gsl_vector* y, gsl_vector* f_call){
	gsl_vector_set(f_call,1,f_call->data[1]+1);
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 2*(1-x_1)*(-1)+100*2*(x_2-x_1*x_1)*(-1)*2*x_1);
	gsl_vector_set(y,1, 100*2*(x_2-x_1*x_1));
}

void himmel(gsl_vector* x, gsl_vector* y, gsl_vector* f_call){
	gsl_vector_set(f_call,2,f_call->data[2]+1);
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(y,0, 4*(x_1*x_1 + x_2 - 11)*x_1 + 2*(x_1 + x_2*x_2 - 7));
	gsl_vector_set(y,1, 2*(x_1*x_1 + x_2 - 11) + 4*(x_1 + x_2*x_2 - 7)*x_2); 
}
