#ifndef HAVE_ROOTS_W_GSL_H
#define HAVE_ROOTS_W_GSL_H
int gsl_root_equation_lineq(const gsl_vector* x, void* params, gsl_vector* f);
int gsl_root_lineq(double* z, double* z2);
int gsl_root_equation_rosen(const gsl_vector* x, void* params, gsl_vector* f);
int gsl_root_rosen(double* z, double* z2);
int gsl_root_equation_himmel(const gsl_vector* x, void* params, gsl_vector* f);
int gsl_root_himmel(double* z, double* z2);
#endif
