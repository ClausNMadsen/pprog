#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>

int gsl_root_equation_lineq(const gsl_vector* x, void* params, gsl_vector* f){
	double x_1 = x->data[0];
	double x_2 = x->data[1];
        double A = 10000;
	gsl_vector_set(f,0, A*x_1*x_2 - 1);
	gsl_vector_set(f,1, exp(-x_1) + exp(-x_2) - 1 - 1.0/A);
	return GSL_SUCCESS;
}

int gsl_root_lineq(double* z, double* z2){
	double eps = 1e-6;
	gsl_multiroot_function F;
	F.f = gsl_root_equation_lineq;
	F.n = 2;
	F.params = NULL;
	gsl_multiroot_fsolver* S;
	S = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,F.n);
	
	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start, 0, *z);
	gsl_vector_set(start, 1, *z2);
	gsl_multiroot_fsolver_set(S,&F,start);

	printf("\nRoot-finding commencing.\n");

	int flag, iter = 0;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,eps);
	}while(flag==GSL_CONTINUE);

	*z = S->x->data[0];
	*z2 = S->x->data[1];

	printf("\nGSL root-finding for the system of equations ended in %i steps at x=(%g,%g).\n",iter,*z,*z2);

	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	return 0;
}




int gsl_root_equation_rosen(const gsl_vector* x, void* params, gsl_vector* f){
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(f,0, 2*(1-x_1)*(-1) + 100*2*(x_2-x_1*x_1)*(-1)*2*x_1);
	gsl_vector_set(f,1, 100*2*(x_2-x_1*x_1));
	return GSL_SUCCESS;
}

int gsl_root_rosen(double* z, double* z2){
	double eps = 1e-6;
	gsl_multiroot_function F;
	F.f = gsl_root_equation_rosen;
	F.n = 2;
	F.params = NULL;
	gsl_multiroot_fsolver* S;
	S = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start, 0, *z);
	gsl_vector_set(start, 1, *z2);
	gsl_multiroot_fsolver_set(S,&F,start);
	
	printf("\nRoot-finding commencing.\n");

	int flag, iter = 0;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,eps);
	}while(flag == GSL_CONTINUE);

	*z = S->x->data[0];
	*z2 = S->x->data[1];
	printf("\nGSL root-finding for the Rosenbrock valley function ended in %i steps at x=(%g,%g).\n",iter,S->x->data[0],S->x->data[1]);

	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	return 0;
}

int gsl_root_equation_himmel(const gsl_vector* x, void* params, gsl_vector* f){
	double x_1 = x->data[0], x_2 = x->data[1];
	gsl_vector_set(f,0, 4*(x_1*x_1 + x_2 - 11)*x_1 + 2*(x_1 + x_2*x_2-7));
	gsl_vector_set(f,1, 2*(x_1*x_1 + x_2 - 11) + 4*(x_1 + x_2*x_2 -7)*x_2);
	return GSL_SUCCESS;
}

int gsl_root_himmel(double* z, double* z2){
	double eps = 1e-6;
	gsl_multiroot_function F;
	F.f = gsl_root_equation_himmel;
	F.n = 2;
	F.params = NULL;
	gsl_multiroot_fsolver* S;
	S = gsl_multiroot_fsolver_alloc(gsl_multiroot_fsolver_hybrids,F.n);

	gsl_vector* start = gsl_vector_alloc(F.n);
	gsl_vector_set(start, 0, *z);
	gsl_vector_set(start, 1, *z2);
	gsl_multiroot_fsolver_set(S,&F,start);

	printf("\nRoot-finding commencing.\n");

	int flag, iter = 0;
	do{
		iter++;
		gsl_multiroot_fsolver_iterate(S);
		flag = gsl_multiroot_test_residual(S->f,eps);
	}while(flag == GSL_CONTINUE);

	*z = S->x->data[0];
	*z2 = S->x->data[1];
	printf("\nGSL root-finding for Himmelblau's function ended in %i steps at x=(%g,%g).\n",iter,*z,*z2);

	gsl_multiroot_fsolver_free(S);
	gsl_vector_free(start);
	return 0;
}

