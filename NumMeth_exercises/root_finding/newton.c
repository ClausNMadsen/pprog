#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_blas.h>
#include"qr_gs.h"

void numjacobian(void f(gsl_vector* x, gsl_vector* fx, gsl_vector* f_call), gsl_matrix* J, gsl_vector* x, gsl_vector* fx, gsl_vector* fx_dx, double dx, gsl_vector* f_call){
	for( int i = 0; i < x->size; ++i){
		double x_i = x->data[i];
		gsl_vector_set(x, i ,x_i + dx);
		f(x, fx_dx,f_call);
		gsl_vector_set(x,i,x_i);

		for(int j = 0; j < x->size; ++j){
			double df_jdx_i = (fx_dx->data[j] - fx->data[j])/dx;
			gsl_matrix_set(J, j, i, df_jdx_i);
		}
	}
}

int newton(void f(gsl_vector* x, gsl_vector* fx, gsl_vector* f_call), gsl_vector* x, double dx, double eps, gsl_vector* f_call){

	int n_eq = x->size;
	gsl_matrix* R = gsl_matrix_alloc(n_eq,n_eq);
	gsl_matrix* J = gsl_matrix_alloc(n_eq,n_eq);
	gsl_vector* fx = gsl_vector_alloc(n_eq);

	gsl_vector* deltax = gsl_vector_alloc(n_eq);
	gsl_vector* new_fx = gsl_vector_alloc(n_eq);
	gsl_vector* cfx = gsl_vector_alloc(n_eq);
	double lambda = 1.00;
	int step = 0;
	double f_norm, new_f_norm;

	do{
		step++;
		f(x,fx,f_call);
		numjacobian(f, J, x, fx, new_fx, dx, f_call);
		gsl_vector_memcpy(cfx,fx);

		qr_gs_decomp(J,R);
		qr_gs_solve(J,R,cfx,deltax);
		lambda = 1.00;
		gsl_vector_scale(deltax,-lambda);
		gsl_vector_add(x,deltax);

		f(x,new_fx,f_call);
		f_norm = gsl_blas_dnrm2(fx);
		new_f_norm = gsl_blas_dnrm2(new_fx);

		while(new_f_norm > (1-lambda/2.00)*f_norm && lambda > 1.00/64.00) {
			lambda /=2.00;
			gsl_vector_scale(deltax,lambda);
			gsl_vector_add(x,deltax);
			f(x,new_fx,f_call);
			new_f_norm = gsl_blas_dnrm2(new_fx);
		}
		f(x,new_fx,f_call);
		new_f_norm = gsl_blas_dnrm2(new_fx);
	}while(new_f_norm > eps);

	gsl_matrix_free(R);
	gsl_matrix_free(J);
	gsl_vector_free(fx);
	gsl_vector_free(deltax);
	gsl_vector_free(cfx);
	gsl_vector_free(new_fx);
	return step;
}
