#ifndef HAVE_NEWTON_W_JACOBIAN_QUAD_INT_H
#define HAVE_NEWTON_W_JACOBIAN_QUAD_INT_H
int newton_w_jacobian_quad_int(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J, gsl_vector* f_call), gsl_vector* x, double eps, gsl_vector* f_call);
#endif
