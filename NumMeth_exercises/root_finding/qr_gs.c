#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_errno.h>
#include<assert.h>
#include<math.h>
#include"qr_gs.h"
#include<gsl/gsl_blas.h>

void qr_gs_decomp(gsl_matrix * A, gsl_matrix * R){
	assert(A->size1 >= A->size2 && A->size2==R->size2 && R->size1==R->size2);
	
	/*loop over columns*/
	for(int j = 0; j<A->size2; ++j){
		
		/*normalize current column + update R diagonal*/
		double Rii = 0;
		for(int i = 0; i<A->size1; ++i){
			Rii += pow(gsl_matrix_get(A,i,j),2);
		}
		Rii = sqrt(Rii);
		gsl_matrix_set(R,j,j,Rii);

		for(int i = 0; i<A->size1; ++i){
			double ai = gsl_matrix_get(A,i,j);
			ai /= gsl_matrix_get(R,j,j);
			gsl_matrix_set(A,i,j,ai);
		}
		for(int k = j+1; k<A->size2; ++k){
			double Rij = 0;
			for(int i = 0; i<A->size1; ++i){
				Rij += gsl_matrix_get(A,i,j)*gsl_matrix_get(A,i,k);
				gsl_matrix_set(R,j,k,Rij);
			}
			for(int i = 0; i<A->size1; ++i){
				double aj = gsl_matrix_get(A,i,k) - gsl_matrix_get(A,i,j)*Rij;
				gsl_matrix_set(A,i,k,aj);
			}
		}
	}
}

void qr_gs_solve(const gsl_matrix* Q, const gsl_matrix* R, const gsl_vector* b, gsl_vector* x){	
	gsl_blas_dgemv(CblasTrans,1.0,Q,b,0.0,x); /*x=1.0*Q^T*b + 0*x*/
	for(int i=R->size1-1;i>=0;--i){ /*back substitution*/
		double s=0;
		for(int j=i+1;j<R->size1;++j){
			s+=gsl_matrix_get(R,i,j)*gsl_vector_get(x,j);
		}
		gsl_vector_set(x,i,(gsl_vector_get(x,i)-s)/gsl_matrix_get(R,i,i));
	}
}

void qr_gs_inverse(const gsl_matrix* Q, const gsl_matrix* R, gsl_matrix* B){
	int vecsize = R->size1;
	gsl_vector* b = gsl_vector_alloc(vecsize);
	gsl_vector* x = gsl_vector_alloc(vecsize);
	for(int i=0; i<b->size; ++i){
		gsl_vector_set(b,i,1.0);
		qr_gs_solve(Q,R,b,x);
		gsl_vector_set(b,i,0.0);
		gsl_matrix_set_col(B,i,x);
	}
	gsl_vector_free(b);
	gsl_vector_free(x);
}

