#ifndef HAVE_ROOT_FINDING_H
#define HAVE_ROOT_FINDING_H
#include"root_test_functions.h"
#include"newton_w_jacobian.h"
#include"newton.h"
#include"roots_w_gsl.h"
#include"newton_w_jacobian_quad_int.h"
#endif
