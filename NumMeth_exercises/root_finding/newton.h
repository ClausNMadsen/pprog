#ifndef HAVE_NEWTON_C
#define HAVE_NEWTON_C
void numjacobian(void f(gsl_vector* x, gsl_vector* fx, gsl_vector* f_call), gsl_matrix* J, gsl_vector* x, gsl_vector* fx, gsl_vector* fx_dx, double dx, gsl_vector* f_call);
int newton(void f(gsl_vector* x, gsl_vector* fx, gsl_vector* f_call), gsl_vector* x, double dx, double eps, gsl_vector* f_call);
#endif
