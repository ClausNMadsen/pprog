#ifndef HAVE_NEWTON_W_JACOBIAN_H
#define HAVE_NEWTON_W_JACOBIAN_H
int newton_w_jacobian(void f(gsl_vector* x, gsl_vector* fx, gsl_matrix* J, gsl_vector* f_call), gsl_vector* x, double eps, gsl_vector* f_call);
#endif
