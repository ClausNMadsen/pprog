#ifndef HAVE_MONTECARLOFUNCTIONS_H
#define HAVE_MONTECARLOFUNCTIONS_H
double f_volume(gsl_vector* a, gsl_vector* b);
void random_x(gsl_vector* a, gsl_vector* b, gsl_vector* x);
int plain_montecarlo(gsl_vector* a, gsl_vector* b, double f(gsl_vector*), int N, double* result, double* error);
int montecarlo_for_recursive(gsl_vector* a, gsl_vector* b, double f(gsl_vector*), int N, double* result, double* var);
int recursive_montecarlo(gsl_vector* a, gsl_vector* b, double f(gsl_vector*), int N, double* result, double* error, double acc, double eps);
#endif
