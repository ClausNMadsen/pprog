#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"montecarlofunctions.h"

int main(void){
	/*Part A*/
	printf("Part A of the exercise initiating.\n");
	printf("\nA plain Monte Carlo integrator based on the lecture notes has been implemented. It takes point a and b to integrate between, the number of points to be used and the function to be integrated and returns the value of the integral and the error estimate.\n");

	/*points to be used for the integrals*/
	int N = 1e8;
	printf("\nFor the first part of part A of this exercise, all integrations will be done using %i shots.\n",N);

	/*First integral*/
	printf("\nThe first test is the integral over the volume element of a sphere to radius 1.\n");
	int dim1 = 3;
	gsl_vector* a1 = gsl_vector_alloc(dim1);
	gsl_vector* b1 = gsl_vector_alloc(dim1);
	gsl_vector_set(a1,0,0);
	gsl_vector_set(a1,1,0);
	gsl_vector_set(a1,2,0);
	gsl_vector_set(b1,0,1);
	gsl_vector_set(b1,1,M_PI);
	gsl_vector_set(b1,2,2*M_PI);
	double result1, error1;
	double f1(gsl_vector* x){
		double r = x->data[0], theta = x->data[1];
		return r*r*sin(theta);
	}
	printf("Initiating integration.\n");
	plain_montecarlo(a1, b1, f1, N, &result1, &error1);
	printf("The calculation finished.\n");
	printf("The result should be 4/3*π=4.18879020478639...\n");
	printf("Monte Carlo integration yielded I=%g with error=%g.\n",result1,error1);


	/*Next integral*/
	printf("\nThe next integral is a spherically symmetric integral from r=0 to r=1 of 1/r.\n");
	        int dim2 = 3;
        gsl_vector* a2 = gsl_vector_alloc(dim2);
        gsl_vector* b2 = gsl_vector_alloc(dim2);
	gsl_vector_set(a2,0,0);
	gsl_vector_set(a2,1,0);
	gsl_vector_set(a2,2,0);
	gsl_vector_set(b2,0,1.0);
	gsl_vector_set(b2,1,M_PI);
	gsl_vector_set(b2,2,2*M_PI);
	double result2, error2;
        double f2(gsl_vector* x){
                double r = x->data[0], theta=x->data[1];
                return r*sin(theta);
        }
	
        printf("Initiating integration.\n");
        plain_montecarlo(a2, b2, f2, N, &result2, &error2);
        printf("The calculation finished.\n");
        printf("The result should be 2*π=6.2831853071...\n");
        printf("Monte Carlo integration yielded I=%g with error=%g.\n",result2,error2);

	
	/*Last integral*/
	printf("\nThe last of these small fun integrals is the one-dimensional integral form the integration exercise, I = ln(x)/x^(1/2)=-4\n");
	int dim3 = 1;
	gsl_vector* a3 = gsl_vector_alloc(dim3);
	gsl_vector* b3 = gsl_vector_alloc(dim3);
	gsl_vector_set(a3,0,0);
	gsl_vector_set(b3,0,1);
	double result3, error3;
	double f3(gsl_vector* x){double x1 = x->data[0];return log(x1)/sqrt(x1);}
	printf("Initiating integration.\n");
        plain_montecarlo(a3, b3, f3, N, &result3, &error3);
        printf("The calculation finished.\n");
        printf("The result should be I=-4\n");
        printf("Monte Carlo integration yielded I=%g with error=%g.\n",result3,error3);

	printf("\n\nIn general, the integrator seems to calculate the intervals to the self-reported estimated accuracy.\n");
	

	/*The difficult provided integral*/
	printf("\nNow to the provided difficult integral. %i points will be used again, as this amount of points proved fairly accurate in the preceding section.\n",N);
	printf("The integral is I =∫ from 0 to π  dx/π ∫ from 0 to π  dy/π ∫ form 0 to π  dz/π of [1-cos(x)cos(y)cos(z)]^-1 = Γ(1/4)^4/(4π^3) ≈ 1.3932039296856768591842462603255.\n");
	int dim4 = 3;
	gsl_vector* a4 = gsl_vector_alloc(dim4);
	gsl_vector* b4 = gsl_vector_alloc(dim4);
	double result4, error4;

	gsl_vector_set(a4, 0, 0.0);
	gsl_vector_set(a4, 1, 0.0);
	gsl_vector_set(a4, 2, 0.0);
	gsl_vector_set(b4, 0, M_PI);
	gsl_vector_set(b4, 1, M_PI);
	gsl_vector_set(b4, 2, M_PI);
	double f4(gsl_vector* x){
		double x1 = x->data[0], y1 = x->data[1], z1 = x->data[2];
		return 1.0/(M_PI*M_PI*M_PI*(1.0 - cos(x1) * cos(y1) * cos(z1)));
	};

	printf("The integral calculation has been initiated.\n");
	plain_montecarlo(a4, b4, f4, N, &result4, &error4);
	printf("The integral should give I≈1.3932039296856768591842462603255.\n");
	printf("Monte Carlo gave I=%g with error=%g.\n",result4,error4);
	printf("\nFairly close to the correct result.\n");

	/*Part B*/
	int dimB = 1;
	printf("\n\nPart B now running.\n");
	printf("To test error scaling, a fairly simple function will be used.\n");
	printf("I = int from 0 to 1 of (exp(x)+1)dx = e = 2.718281828459045235360...\n");
	gsl_vector* aB = gsl_vector_alloc(dimB);
	gsl_vector* bB = gsl_vector_alloc(dimB);
	double resultB, errorB;
	gsl_vector_set(aB,0,0.0);
	gsl_vector_set(bB,0,1.0);
	double fB(gsl_vector* x){
		double x1 = x->data[0];
		return exp(x1)+1;
	}
	printf("Calculations started.\n");
	for(double N = 1; N<7; N+=0.05){
		plain_montecarlo(aB, bB, fB, (int)round(pow(10,N)), &resultB, &errorB);
		fprintf(stderr, "%i %g\n", (int)round(pow(10,N)),fabs(errorB));
	}
	printf("Calculation done.\n");
	printf("\nA plot of the absolute error of the Monte Carlo integration as a function of 1/sqrt(number of shots) has been generated in plot.svg.\n");
	printf("This concludes part B of the exercise.\n");


	/*Part C*/
	printf("\nNow for part C of the exercise.\n");
	printf("A Monte Carlo integrator utilizing recursive stratified sampling has been implemented.\n");

	printf("The test of this iteration of the integrator will be calculating some of the preceding integrals.\n");
	
	int N_rec = 1000; 
	double acc = 1e-2, eps = 1e-2;
       
	printf("First integral running.\n");
	double resultC1, errorC1;
	recursive_montecarlo( a1, b1, f1, N_rec, &resultC1, &errorC1, acc, eps);
	printf("The calculation finished.\n");
        printf("The result should be 4/3*π=4.18879020478639...\n");
        printf("Recursive Monte Carlo integration yielded I=%g with error=%g.\n",resultC1,errorC1);

	printf("\nSecond integral running.\n");
	double resultC2, errorC2;
	recursive_montecarlo(a2, b2, f2, N_rec, &resultC2, &errorC2, acc, eps);
	printf("The calculation finished.\n");
        printf("The result should be 2*π=6.2831853071...\n");
        printf("Recursive Monte Carlo integration yielded I=%g with error=%g.\n",resultC2,errorC2);

	printf("\nI refrain from testing the difficult provided integral, as it would be very time consuming.\n");

	printf("\nA recursive stratified Monte Carlo integrator has been implemented and demonstrated to work. Part C has thus been completed.\n");
	printf("\nThis concludes part C of this exercise and thus the whole exercise.\n");
	gsl_vector_free(a1);
	gsl_vector_free(b1);
	gsl_vector_free(a2);
	gsl_vector_free(b2);
	gsl_vector_free(a3);
	gsl_vector_free(b3);
	gsl_vector_free(a4);
	gsl_vector_free(b4);
	gsl_vector_free(aB);
	gsl_vector_free(bB);
	return 0;
}
