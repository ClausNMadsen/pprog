#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"montecarlofunctions.h"

void random_x(gsl_vector* a, gsl_vector* b, gsl_vector* x){
	/*This random function is ripped from the lecture notes, incorporating gsl vectors*/
	/*This function generates a random point between points a and b*/
	for(int i = 0; i < a->size; ++i){
		double a_i = a->data[i], b_i = b->data[i];
		double RND = (double)rand()/RAND_MAX;
		double x_i = a_i + RND * (b_i - a_i);
		gsl_vector_set(x, i, x_i);
	}
}

int plain_montecarlo(gsl_vector* a, gsl_vector* b, double f(gsl_vector*), int N, double* result, double* error){
	/*Plain Monte Carlo integrator ripped from the lecture notes, incorporating gsl vectors*/
	/*Takes points a and b to integrate the function f between, the number of points to use in the integration and returns the integration result and estimated error*/

	double V = f_volume(a,b);
	gsl_vector* x = gsl_vector_alloc(a->size);
	double sum = 0, sum_sq = 0;

	for(int i = 0; i < N; ++i){
		random_x(a, b, x);
		double fx = f(x);
		sum += fx;
		sum_sq += fx*fx;
	}

	/*Average value and variance from sum and sum squared*/
	double avr = sum/N;
	double var = sum_sq/N - avr*avr;
	*result = avr*V;
	*error = sqrt(var/N)*V;
	gsl_vector_free(x);
	return 0;
}

double f_volume(gsl_vector* a, gsl_vector* b){
	/*returns volume between points a and b*/
	double V = 1;
	for(int i = 0; i < a->size; ++i){
		double a_i = a->data[i], b_i = b->data[i];
		V *= b_i - a_i;
	}
	return V;
}

int montecarlo_for_recursive(gsl_vector* a, gsl_vector* b, double f(gsl_vector*), int N, double* result, double* var){
	double V = f_volume(a,b);
	double sum=0, sum_sq=0;
	gsl_vector* x = gsl_vector_alloc(a->size);

	for(int i = 0; i<N; ++i){
		random_x(a, b, x);
		double fx = f(x);
		sum += fx;
		sum_sq += fx*fx;
	}

	double avr = sum/N;
	*var = sum_sq/N - avr*avr;
	*result = avr*V;
	gsl_vector_free(x);
	return 0;
}

int recursive_montecarlo(gsl_vector* a, gsl_vector* b, double f(gsl_vector*), int N, double* result, double* error, double acc, double eps){
	/*The recursive Monte Carlo is made as defined in the lecture notes.*/

	gsl_vector* sub_variance_vector1 = gsl_vector_alloc(a->size);
	gsl_vector* sub_variance_vector2 = gsl_vector_alloc(a->size);
	double result0, var0, result1, var1, result2, var2;

	montecarlo_for_recursive(a,b,f,N,&result0,&var0);

	double V = f_volume(a,b);
	*result = result0;
	*error = sqrt(var0/(double)N)*V;

	if(*error < acc + eps * *result){
		gsl_vector_free(sub_variance_vector1);
		gsl_vector_free(sub_variance_vector2);
		return 0;
	}
	else{
		gsl_vector* a_avg = gsl_vector_alloc(a->size);
		gsl_vector* b_avg = gsl_vector_alloc(a->size);
		double a_i, b_i, test, ab_average;

		for(int dim = 0; dim < a->size; ++dim){
			a_i = a->data[dim], b_i = b->data[dim];
			ab_average = (a_i + b_i)/2.0;
			gsl_vector_memcpy(a_avg,a);
			gsl_vector_memcpy(b_avg,b);
			gsl_vector_set(a_avg,dim,ab_average);
			gsl_vector_set(b_avg,dim,ab_average);
			/*Giving some delay to avoid seg faults*/
			test = 0.0;
			a_i += test;

			montecarlo_for_recursive(a,b_avg,f,N,&result1,&var1);
			montecarlo_for_recursive(a_avg,b,f,N,&result2,&var2);

			if(var1 < 0){
				var1 = -var1;
			}
			if(var2 < 0){
				var2 = -var2;
			}
			gsl_vector_set(sub_variance_vector1,dim,var1);
			gsl_vector_set(sub_variance_vector2,dim,var2);
		}
		gsl_vector_add(sub_variance_vector1,sub_variance_vector2);
		int max_variance_index = gsl_vector_max_index(sub_variance_vector1);

		gsl_vector_memcpy(a_avg, a);
		gsl_vector_memcpy(b_avg, b);

		a_i = a->data[max_variance_index];
		b_i = b->data[max_variance_index];
		ab_average = (b_i - a_i)/2.0 + a_i;

		gsl_vector_set(a_avg,max_variance_index,ab_average);
		gsl_vector_set(b_avg,max_variance_index,ab_average);

		recursive_montecarlo(a,b_avg,f,N,&result1,&var1,acc/1.414,eps);
		recursive_montecarlo(a_avg,b,f,N,&result1,&var1,acc/1.414,eps);

		*result = result1 + result2;
		*error = sqrt(var1 * var1 + var2 * var2);

		
		gsl_vector_free(a_avg);
		gsl_vector_free(b_avg);
		gsl_vector_free(sub_variance_vector1);
		gsl_vector_free(sub_variance_vector2);
		
		return 0;
	}
}	


