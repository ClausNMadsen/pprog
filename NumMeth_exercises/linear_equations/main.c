#include<stdio.h>
#include<stdlib.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_errno.h>
#include"qr_gs.h"
#include<gsl/gsl_blas.h>
#include<float.h>

int main(){

	/*making a tall matrix with random number entries*/
	double size1 = 4;
	double size2 = 3;
	gsl_matrix* A = gsl_matrix_alloc(size1,size2);
       	for(int i=0; i<size1; ++i){
		for(int j=0; j<size2; ++j){
			gsl_matrix_set(A,i,j,(double)rand()/RAND_MAX*10.0);
		}
	}
	/*copying A, since my QR destroys it*/
	gsl_matrix* A_copy = gsl_matrix_alloc(size1,size2);	
	gsl_matrix_memcpy(A_copy,A);

	/*running QR-factorization*/
	gsl_matrix* R = gsl_matrix_alloc(size2,size2);
	qr_gs_decomp(A,R);

	printf("\nBefore QR-decomp, A is\nA=");
	for(int i=0; i<size1; ++i){
		printf("\t%.2g\t%.2g\t%.2g\n",gsl_matrix_get(A_copy,i,0),gsl_matrix_get(A_copy,i,1),gsl_matrix_get(A_copy,i,2));
	}

	printf("\nAfter QR-decomp, Q and R are:\nQ=");
	for(int i=0; i<size1; ++i){
		printf("\t%.2g\t%.2g\t%.2g\n",gsl_matrix_get(A,i,0),gsl_matrix_get(A,i,1),gsl_matrix_get(A,i,2));
		
	}
	printf("\nR=");
	for(int i=0;i<size2; ++i){
		printf("\t%.2g\t%.2g\t%.2g\n",gsl_matrix_get(R,i,0),gsl_matrix_get(R,i,1),gsl_matrix_get(R,i,2));
	}
	
	/*testing Q^T*Q and QR = A*/

	printf("\nQ^T*Q is\nQ^T*Q=");
	gsl_matrix* QTQ = gsl_matrix_alloc(size2,size2);
	gsl_blas_dgemm(CblasTrans,CblasNoTrans,1,A,A,0,QTQ);

	for(int i=0; i<size2; ++i){
		printf("\t%.2g\t%.2g\t%.2g\n",gsl_matrix_get(QTQ,i,0),gsl_matrix_get(QTQ,i,1),gsl_matrix_get(QTQ,i,2));
	}

	gsl_matrix* QR = gsl_matrix_alloc(size1,size2);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1,A,R,0,QR);

	printf("\nTesting QR\nQR=A\n");
	for(int i=0; i<size1; ++i){
		printf("\t%.2g\t%.2g\t%.2g\t=\t%.2g\t%.2g\t%.2g\n",\
				gsl_matrix_get(QR,i,0),\
				gsl_matrix_get(QR,i,1),\
				gsl_matrix_get(QR,i,2),\
				gsl_matrix_get(A_copy,i,0),\
				gsl_matrix_get(A_copy,i,1),\
				gsl_matrix_get(A_copy,i,2));
	}
	
	/*testing QR-solver*/
	/*preallocating square matrices A and R for the test*/
	int square_m_size = 4;
	gsl_matrix* A_2 = gsl_matrix_alloc(square_m_size,square_m_size);
	for(int i=0; i<A_2->size1; ++i){
                for(int j=0; j<A_2->size2; ++j){
                        gsl_matrix_set(A_2,i,j,(double)rand()/RAND_MAX*10.0);
                }
        }
	gsl_matrix* R_2 = gsl_matrix_alloc(A_2->size1,A_2->size2);
	gsl_matrix* A_2_copy = gsl_matrix_alloc(A_2->size1,A_2->size2);
	gsl_matrix_memcpy(A_2_copy,A_2);
	
	/*allocating vectors b and x*/
	gsl_vector* b_2 = gsl_vector_alloc(A_2->size1);
	for(int i=0; i<b_2->size; ++i){
	       gsl_vector_set(b_2,i,(double)rand()/RAND_MAX*10.0);
	}	       
	gsl_vector* x_2 = gsl_vector_alloc(b_2->size);

	/*factorizing A=QR*/
	qr_gs_decomp(A_2,R_2);

	/*solving QRx=b*/
	qr_gs_solve(A_2,R_2,b_2,x_2);
	
	printf("\nTesting the QR linear equation solver\n");
	printf("Before solving, random square matrix A is\n");
	

	for(int i=0;i<A_2_copy->size1;++i){
		for(int j=0;j<A_2_copy->size2;++j){
			printf("%7.3g",gsl_matrix_get(A_2_copy,i,j));;
		}
		printf("\n");
	}
	printf("Random vector b is\n");
	for(int i=0; i<b_2->size; ++i){
		printf("%7.3g\n",b_2->data[i]);
	}
	
	printf("Solution vector x, from Ax=b, is found to be\n");
	for(int i=0; i<x_2->size; ++i){
		printf("%7.3g\n",x_2->data[i]);
	}

	gsl_vector* b_true = gsl_vector_alloc(b_2->size);
	gsl_blas_dgemv(CblasNoTrans,1.0,A_2_copy,x_2,0.0,b_true);
	
	printf("Using this x and a copy of the original A, Ax should equal b\nAx=\n");	
	for(int i=0; i<b_true->size; ++i){
		printf("%7.3g\n",b_true->data[i]);
	}	
	
	printf("\n-------B------\n");

	/*generating new square matrix with random entries*/

	int size_B = 5; 
	gsl_matrix* A_B = gsl_matrix_alloc(size_B,size_B);
	for(int i=0; i<A_B->size1; ++i){
		for(int j=0; j<A_B->size2; ++j){
			gsl_matrix_set(A_B,i,j,(double)rand()/RAND_MAX*10.0);
		}
	}
	gsl_matrix* A_B_copy = gsl_matrix_alloc(size_B,size_B);
	gsl_matrix* R_B = gsl_matrix_alloc(size_B,size_B);
	gsl_matrix* B_B = gsl_matrix_alloc(size_B,size_B);
	gsl_matrix_memcpy(A_B_copy,A_B);

	qr_gs_decomp(A_B,R_B);
	qr_gs_inverse(A_B,R_B,B_B);

	printf("\nThe random square matrix for part B is\nA=\n");
	for(int i=0; i<A_B_copy->size1; ++i){
		for(int j=0; j<A_B_copy->size2; ++j){
			printf("%7.3g",gsl_matrix_get(A_B_copy,i,j));
		}
		printf("\n");
	}

	printf("\nMy QR decomposition gives Q and R\nQ=\n");
	for(int i=0; i<A_B->size1; ++i){
		for(int j=0; j<A_B->size2; ++j){
			printf("%7.3g",gsl_matrix_get(A_B,i,j));
		}
		printf("\n");
	}
	
	printf("\nR=\n");
	for(int i=0; i<R_B->size1; ++i){
		for(int j=0; j<R_B->size2; ++j){
			printf("%7.3g",gsl_matrix_get(R_B,i,j));
		}
		printf("\n");
	}

	printf("\nThe inverse matrix A^-1 is\n");
	for(int i=0; i<B_B->size1; ++i){
		for(int j=0; j<B_B->size2; ++j){
			printf("%7.3g",gsl_matrix_get(B_B,i,j));
		}
		printf("\n");
	}
	/*testing A^-1 (which is B_B) times A (which is A_B_copy)*/
	gsl_matrix* AinvA = gsl_matrix_alloc(size_B,size_B);
	gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,B_B,A_B_copy,0.0,AinvA);

	/*tidying up extremely low values which should be 0.0*/
	for(int i=0; i<AinvA->size1; ++i){
		for(int j=0; j<AinvA->size2; ++j){
			if(gsl_matrix_get(AinvA,i,j)<DBL_EPSILON*20){
				gsl_matrix_set(AinvA,i,j,0.0);
			}
		}
	}
	printf("\nThe product A^-1*A should be 1. Checking:\n");
	for(int i=0; i<AinvA->size1; ++i){
		for(int j=0; j<AinvA->size2; ++j){
			printf("%7.4g",gsl_matrix_get(AinvA,i,j));
		}
		printf("\n");
	}

	/*free all used malloc mem*/
	gsl_matrix_free(A);
	gsl_matrix_free(R);
	gsl_matrix_free(A_copy);
	gsl_matrix_free(QTQ);
	gsl_matrix_free(A_2);
	gsl_matrix_free(A_2_copy);
	gsl_matrix_free(R_2);
	gsl_vector_free(b_2);
	gsl_vector_free(b_true);
	gsl_matrix_free(A_B);
	gsl_matrix_free(R_B);
	gsl_matrix_free(B_B);
	gsl_matrix_free(A_B_copy);
	return 0;
}
