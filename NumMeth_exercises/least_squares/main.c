#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include"qr_gs.h"

void lsfit(int m, double f(int i, double x), gsl_vector* x, gsl_vector* y,
		gsl_vector* dy, gsl_vector* c, gsl_matrix* S);

int main(void){
	printf("Exercise A\n");
	/*data from assignment*/
	double x[] = {0.1,1.33,2.55,3.78,5,6.22,7.45,8.68,9.9};
	double y[] = {-15.3,0.32,2.45,2.75,2.27,1.35,0.157,-1.23,-2.75};
	double dy[]= {1.04,0.594,0.983,0.998,1.11,0.398,0.535,0.968,0.478};
	/*length of the data arrays, assuming equal length*/
	const int n = sizeof(x)/sizeof(x[0]);

	/*printing data to plotting file*/
	FILE* file = fopen("data_A.txt","w");
	for(int i=0; i<n; i++){
		fprintf(file,"%g %g %g\n",x[i],y[i],dy[i]);
	}
	fprintf(file,"\n\n");

	/*making gsl-vectors from the data arrays*/
	gsl_vector* vx = gsl_vector_alloc(n);
	gsl_vector* vy = gsl_vector_alloc(n);
	gsl_vector* vdy= gsl_vector_alloc(n);
	for(int i=0; i<n; i++){
		gsl_vector_set(vx,i,x[i]);
		gsl_vector_set(vy,i,y[i]);
		gsl_vector_set(vdy,i,dy[i]);
	}
	
	/*making a vector of functions to fit to*/
	int m=3;
	double funs(int i, double x){
		switch(i){
			case 0: return log(x);
			case 1: return 1;
			case 2: return x;
			default: return NAN;
		}
	}

	printf("Attempting to fit by QR-decomposition:\n");

	/*initiating fit result matrices*/
	gsl_vector* c = gsl_vector_alloc(m);
	gsl_matrix* S = gsl_matrix_alloc(m,m);
	/*calling the least squares fitting function for both fits and covariances*/
	lsfit(m,funs,vx,vy,vdy,c,S);
	
	printf("Fitting completed.\n");
	printf("The fitting procedure returned solution vector c =\n");
	for(int i=0; i<c->size; i++){
		printf("%7.3g\n",c->data[i]);
	}
	printf("\n\nThe fitting procedure returned covariance matrix S =\n");
	for(int i=0; i<S->size1; i++){
		for(int j=0; j<S->size2; j++){
			printf("%9.4g",gsl_matrix_get(S,i,j));
		}
		printf("\n");
	}


	
	/*definition of the fitted line*/
	double fit(double x){
		double s=0;
		for(int k=0; k<m; k++){
			s+=gsl_vector_get(c,k)*funs(k,x);
		}
		return s;
	}

	/*printing of fitted line to data file*/
	const double step = (vx->data[n-1]-vx->data[0])/200; 

	for(double z = vx->data[0]; z<vx->data[n-1]; z+=step){
		double y = fit(z);
		fprintf(file, "%g %g\n",z,y);
	}

	printf("Experimental data is plotted along with the fitted line in plot_A.svg. The fit is seen to be accurate.\n");
	printf("A plot to show the linear least squares fitting results is found as the file plot.svg.\n");

	printf("\n\nExercise B\n");
	printf("Since the fitting procedure is based on the example given at the lecture, a calculation of the covariance matrix is already included. The result is shown in part A.\n");

	
	/*I open a new file for the plotting data for part B*/
	FILE* fileB = fopen("data_B.txt","w");
	
	for(double z = vx->data[0]; z<vx->data[n-1]; z+=step){
		double f = 0.0;
		double df = 0.00;
		for(int i = 0; i<c->size; i++){
			double c_i = gsl_vector_get(c,i);
			for(int j=0; j<c->size; j++){
			       df += gsl_matrix_get(S,i,j) * funs(i,z)* funs(j,z);
			}
			f += c_i*funs(i,z);
		}
		fprintf(fileB,"%g %g %g\n",z,f, sqrt(df));
	}

	printf("The estimated error is plotted along with experimental data and the fitted line in plot_B.svg\n");
	printf("It is seen that the estimated error bounds of the fitted line fits decently within the error estimates on the measured points.\n");
	
	
	gsl_vector_free(vx);
	gsl_vector_free(vy);
	gsl_vector_free(vdy);
	gsl_vector_free(c);
	gsl_matrix_free(S);
	return 0;
}
