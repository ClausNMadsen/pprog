#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"ode.h"

int main(void){
	
	double theta_1_init = 3.14*0.75;
	double theta_2_init = 3.14*0.75;
	double p_1_init = 0;
	double p_2_init = 0;

	/*Part A*/
	printf("Part A initializing.\n");
	printf("An integrator based on an the Runge-Kutta method has been implemented.\n");
	printf("I thought a fun test would be a double pendulum system of ODEs.\n");
	printf("Runge-Kutta12 initializing and driving.\n");
	double x, h, acc = 0.0, eps = 1e-3;
	gsl_vector* q = gsl_vector_calloc(4);
	
	for(double t = 0; t<20; t+=0.01){
		gsl_vector_set(q,0,theta_1_init);
		gsl_vector_set(q,1,theta_2_init);
		gsl_vector_set(q,2,p_1_init);
		gsl_vector_set(q,3,p_2_init);
		h = 0.01*t/fabs(t);
		x = 0;

		driver(&x, t, &h, q, acc, eps, &rkstep12,&ode_double_pendulum);
		if(t>0){fprintf(stderr, "%g %g %g %g %g\n", x, q->data[0], q->data[1], q->data[2], q->data[3]);}
	}
	printf("A time advanced solution is plotted in plot_A.svg.\n");


	/*Part B*/
	
	printf("Part B of exercise initializing.\n");
	printf("A modified driver is implemented which stores the parth in a matrix, thus avoiding the need for a for-loop in the mainfile.\n");
	
	double b = 50;
	int pathlength = 0;
	x=0, acc = 0.0, eps = 1e-3;
	gsl_vector* y_B = gsl_vector_alloc(4);
	gsl_matrix* y_path = gsl_matrix_alloc(10000,5);
	printf("The modified Runge-Kutta12 is now running.\n");

	gsl_vector_set(y_B,0,theta_1_init);
	gsl_vector_set(y_B,1,theta_2_init);
	gsl_vector_set(y_B,2,p_1_init);
	gsl_vector_set(y_B,3,p_2_init);
	
	h = 0.01*b/fabs(b);
	x = 0;
	driver_mod(&x, b, &h, y_B, acc, eps, &rkstep12, &ode_double_pendulum, y_path,&pathlength);
	fprintf(stderr,"\n\n\n");

	for(int i = 0; i < pathlength; ++i){
		fprintf(stderr,"%g %g %g %g %g\n",gsl_matrix_get(y_path,i,0),gsl_matrix_get(y_path,i,1),gsl_matrix_get(y_path,i,2),gsl_matrix_get(y_path,i,3),gsl_matrix_get(y_path,i,4));
	}

	printf("Modified Runge-Kutta12 finished.\n");
	printf("Having implemented the second driver, part B of the exercise has concluded.\n");
	
	return 0;
}
