#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"rungekutta12.h"

void rkstep12(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err){
	/*This Runge-Kutta 1-2 method is more or less lifted from the lecture notes*/
	f(t, yt, err);
	gsl_vector_scale(err,h/2.0);
	gsl_vector_add(yt,err);


	f(t+h/2.0, yt, yth);

	gsl_vector_scale(err, -1);
	gsl_vector_add(yt, err);

	gsl_vector_scale(err, -1);
	gsl_vector_scale(yth, -h/2.0);
	gsl_vector_add(err, yth);
	gsl_vector_scale(yth, -2.0/h);

	gsl_vector_scale(yth,h);
	gsl_vector_add(yth,yt);
}

void driver(
		double* t,
		double b,
		double* h,
		gsl_vector* yt,
		double acc,
		double eps,
		void stepper(
				double t, double h, gsl_vector* yt,
				void f(double t, gsl_vector* y, gsl_vector* dydt),
				gsl_vector* yth, gsl_vector* err
			    ),
		void f(double t, gsl_vector* y, gsl_vector* dydt)
		){
	int step = 0, n = yt->size, start = *t;
	double error, normy, tol;
	gsl_vector* y_step = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);

	do{
		if(fabs(*t + *h) > fabs(b)) {*h = b - *t;}

		stepper(*t, *h, yt, f, y_step, err);
		error = gsl_blas_dnrm2(err);
		normy = gsl_blas_dnrm2(yt);
		tol = (normy*eps + acc)*sqrt(*h/(b-start));

		if(tol > error){
			step++;
			gsl_vector_memcpy(yt,y_step);
			*t = *t + *h;
		}

		*h *= pow(tol/error, 0.25)* 0.95;
	}while(fabs(*t-b) > 1e-12 && step < 1e6);

	gsl_vector_free(y_step);
	gsl_vector_free(err);
}

void driver_mod(
		double* t,
		double b,
		double* h,
		gsl_vector* yt,
		double acc,
		double eps,
		void stepper(
			double t, double h, gsl_vector* yt,
			void f(double t, gsl_vector* y, gsl_vector* dydt),
			gsl_vector* yth, gsl_vector* err
			),
		void f(double t, gsl_vector* y, gsl_vector* dydt),
		gsl_matrix* path,
		int* pathlength
	       ){
	int step = 0;
	double error, normy, tol;
	int n = yt->size;
	gsl_vector* y_step = gsl_vector_alloc(n);
	gsl_vector* err = gsl_vector_alloc(n);
	double start = *t;

	do{
	
		if(fabs(*t + *h) > fabs(b)) {*h = b - *t;}

		stepper(*t, *h, yt, f, y_step, err);

		error = gsl_blas_dnrm2(err);
		normy = gsl_blas_dnrm2(yt);
		tol = (normy*eps + acc)*sqrt(*h/(b-start));

		/*error to tolerance check*/
		if(tol > error){
			step++;
			gsl_vector_memcpy(yt,y_step);
			*t = *t + *h;

			/*store to solver*/
			if (*pathlength < path->size1){
				gsl_matrix_set(path, *pathlength, 0, *t-*h);
				for(int i=0; i<(yt->size); ++i){
					gsl_matrix_set(path, *pathlength, i+1, yt->data[i]);
				}
				(*pathlength)++;
			}
		}
		/*new error*/
		*h *= pow(tol/error, 0.25)*0.95;
	}while(fabs(*t -b) >1e-12 && step < 1e6);

	gsl_vector_free(y_step);
	gsl_vector_free(err);
}

void integration_by_ode(
		double* t,
		double b,
		double* h,
		double acc,
		double eps,
		void stepper(
				double t, double h, gsl_vector* yt,
				void f(double t, gsl_vector* y, gsl_vector* dydt),
				gsl_vector* yth, gsl_vector* err
			     ),
			double function(double t),
			double* result
		    ){
	void function_in_integral(double t, gsl_vector* y, gsl_vector* f_val){
		gsl_vector_set(f_val, 0, function(t));
	}

	gsl_vector* yt = gsl_vector_alloc(1);
	gsl_vector_set(yt, 0.0, 0.0);

	driver(t, b, h, yt, acc, eps, stepper, function_in_integral);

	*result = yt->data[0];

	gsl_vector_free(yt);
}

	
