#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>
#include"test_ode.h"

void ode_double_pendulum(double t, gsl_vector* q, gsl_vector* dqdt){
	double theta_1 = q->data[0], theta_2 = q->data[1], p_1 = q->data[2], p_2 = q->data[3];
	/*evalutate*/
	double dtheta_1dt = 6*(2*p_1-3*cos(theta_1-theta_2)*p_2)/(16-9*cos(theta_1-theta_2)*cos(theta_1-theta_2));
	double dtheta_2dt = 6*(8*p_2-3*cos(theta_1-theta_2)*p_1)/(16-9*cos(theta_1-theta_2)*cos(theta_1-theta_2));

	gsl_vector_set(dqdt, 0, dtheta_1dt);
	gsl_vector_set(dqdt, 1, dtheta_2dt);
	gsl_vector_set(dqdt, 2, -1.0/2*(dtheta_1dt*dtheta_2dt*sin(theta_1-theta_2) + 3*sin(theta_1)));
	gsl_vector_set(dqdt, 3, -1.0/2*(-dtheta_1dt*dtheta_2dt*sin(theta_1-theta_2) + sin(theta_2)));
}

