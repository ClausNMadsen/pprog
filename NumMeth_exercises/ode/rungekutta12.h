#ifndef HAVE_RUNGEKUTTA12_H
#define HAVE_RUNGEKUTTA12_H
void rkstep12(double t, double h, gsl_vector* yt, void f(double t, gsl_vector* y, gsl_vector* dydt), gsl_vector* yth, gsl_vector* err);
void driver(double* t,double b,double* h,gsl_vector* yt,double acc,double eps,
                void stepper(
                                double t, double h, gsl_vector* yt,
                                void f(double t, gsl_vector* y, gsl_vector* dydt),
                                gsl_vector* yth, gsl_vector* err
                            ),
                void f(double t, gsl_vector* y, gsl_vector* dydt)
                );
void driver_mod(double* t, double b, double* h, gsl_vector* yt, double acc,double eps,
		void stepper(
                        double t, double h, gsl_vector* yt,
                        void f(double t, gsl_vector* y, gsl_vector* dydt),
                        gsl_vector* yth, gsl_vector* err
                        ),
		void f(double t, gsl_vector* y, gsl_vector* dydt),gsl_matrix* path,
		int* pathlength
               );
void integration_by_ode(double* t, double b, double* h, double acc, double eps,
			void stepper(
                                double t, double h, gsl_vector* yt,
                                void f(double t, gsl_vector* y, gsl_vector* dydt),
                                gsl_vector* yth, gsl_vector* err
                             ),
			double function(double t),
			double* result
                    );
void function_in_integral(double t, gsl_vector* y, gsl_vector* f_val);

#endif
