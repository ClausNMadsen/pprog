#ifndef HAVE_TEST_ODE_H
#define HAVE_TEST_ODE_H
void ode_double_pendulum(double t, gsl_vector* q, gsl_vector* dqdt);
#endif
