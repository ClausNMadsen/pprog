#include<stdio.h>
#include<stdlib.h>
#include"cspline.h"
#include"qspline.h"
#include"linspline.h"
#include<gsl/gsl_interp.h>
#include<gsl/gsl_spline.h>

int main(){

	/*this block reads x and y data for the logistic function test points*/
	int data_points;
	int scan_int = scanf("%d",&data_points);
	if(scan_int ==0){printf("scanf of number of data points failed!\n");}
	double x[data_points];
	double y[data_points];

	for(int i=0; i<data_points;i++){
		scan_int += scanf("%lg %lg",&x[i],&y[i]);
		}
	if(scan_int == 0){printf("scanf of data points failed!\n");}
	/*choose number of points for evaluated interpolation*/
	int splin_points = 500;
	double step = (x[data_points-1]-x[0])/splin_points;


	/*this block does interpolation and integration*/
	qspline* s_quad = qspline_alloc(data_points,x,y);
	cspline* s_cube = cspline_alloc(data_points,x,y); 

	gsl_interp_accel *acc = gsl_interp_accel_alloc();
	const gsl_interp_type *t = gsl_interp_cspline;
	gsl_spline *gsl = gsl_spline_alloc(t,data_points);
	gsl_spline_init(gsl,x,y,data_points);

	for(int i = 0; i<splin_points;i++){
		double x_splin = step*i;
		double y_lin_splin = lin_interp(data_points,x,y,x_splin);
		double y_lin_splin_int = lin_interp_integrate(data_points,x,y,x_splin);
		double y_lin_splin_derive = lin_interp_derive(data_points,x,y,x_splin);
		fprintf(stdout,"%g %g %g %g %g %g %g %g %g %g %g %g %g\n",\
				x_splin,y_lin_splin,\
				y_lin_splin_int,y_lin_splin_derive,\
				qspline_eval(s_quad,x_splin),\
				qspline_integrate(s_quad,x_splin),\
				qspline_derive(s_quad,x_splin),\
				cspline_eval(s_cube,x_splin),\
				cspline_integrate(s_cube,x_splin),\
				cspline_derive(s_cube,x_splin),
				gsl_spline_eval(gsl,x_splin,acc),
				gsl_spline_eval_deriv(gsl,x_splin,acc),
				gsl_spline_eval_integ(gsl,x[0],x_splin,acc));
	}

	qspline_free(s_quad);
	cspline_free(s_cube);
	gsl_spline_free(gsl);
	gsl_interp_accel_free(acc);

	return 0;
}
