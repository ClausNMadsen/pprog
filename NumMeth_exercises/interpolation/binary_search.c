int binary_search(int n, double * x, double z){

	int i = 0;
	int j = n-1;

	while ( j-i > 1 ){
		int m = (i+j)/2;
		if(z >= x[m]) i = m;
		else j = m;
	}
	return i;
}
		
