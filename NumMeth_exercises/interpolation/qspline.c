#include<stdlib.h>
#include<assert.h>
#include"qspline.h"
#include<math.h>
int binary_search(int n, double * x, double z);

/* qspline struct definiton: {int n, double *x, *y, *b, *c;} qspline*/
qspline* qspline_alloc(int n, double* x, double* y){ /*allocates spline struct*/
	qspline* s = malloc(sizeof(qspline));
	s->b = malloc((n-1)*sizeof(double));
	s->c = malloc((n-1)*sizeof(double));
	s->x = malloc(n*sizeof(double));
	s->y = malloc(n*sizeof(double));
	s->n = n;
	for(int i=0;i<n;++i){
		s->x[i]=x[i];
		s->y[i]=y[i];
	}
	int i;
	double p[n-1], h[n-1];
	for(i=0;i<n-1;i++){
		h[i]=x[i+1]-x[i]; /*x step size*/
		p[i]=(y[i+1]-y[i])/h[i]; /*local tangent slope*/
	}
	s->x[0]=0; /*up recursion, normal boundary condition*/
	for(i=0;i<n-2;i++){
		s->c[i+1]=(p[i+1]-p[i]-s->c[i]*h[i])/h[i+1];
	}
	s->c[n-2]/=2; /*down recursion start, average recursions*/
	for(i=n-3;i>=0;i--){
		s->c[i]=( p[i+1]-p[i]-s->c[i+1]*h[i+1])/h[i];
	}
	for(i=0;i<n-1;i++)
		s->b[i] = p[i]-s->c[i]*h[i];
	/*free(p);
	*free(h); */
	return s;
}
	
double qspline_eval(qspline *s, double z){ /*returns s(z)=interpolated function(z)*/
	assert(z>=s->x[0] && z<=s->x[s->n-1]);
	int i = binary_search(s->n, s->x, z); /*calling binary_search function*/
	double h=z-s->x[i];
	return s->y[i]+h*(s->b[i]+h*s->c[i]);
}

double qspline_integrate(qspline* s, double z){
	int i = binary_search(s->n, s->x, z);
	double value = 0;
	int j;
	for(j=0;j<i;++j){
		double h = s->x[j+1]-s->x[j];
		value += s->y[j]*(h) + 0.5*s->b[j]*(h*h) + 1.0/3*s->c[j]*(h*h*h);
	}
	double h = z - s->x[i];
	value += s->y[i]*h + 1.0/2*s->b[i]*h*h + 1.0/3*s->c[i]*h*h*h;
	return value;
}
	
double qspline_derive(qspline* s, double z){
	int i = binary_search(s->n, s->x, z);
	double h= z-s->x[i];
	return s->b[i]+2*h*s->c[i];

}

void qspline_free(qspline *s){ /*free an allocated qspline*/
	free(s->x);
	free(s->y);
	free(s->b);
	free(s->c);
	free(s);
}
