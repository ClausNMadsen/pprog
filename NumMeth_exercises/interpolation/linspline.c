#include"linspline.h"
#include<assert.h>
int binary_search(int n, double * x, double z);

double lin_interp(int n, double * x, double * y, double z){ /*linear interpolation*/
        assert( n>1 && z>=x[0] && z<=x[n-1]); /*interpolation only in intervals*/

        int i = binary_search(n,x,z);
        double bi=(y[i+1]-y[i])/(x[i+1]-x[i]);
        return y[i]+bi*(z-x[i]);
}

/*integral of linear interpolation from 0 to z*/
double lin_interp_integrate(int n, double * x, double * y, double z){
        assert( n>1 && z>=x[0] && z<=x[n-1]);

        int i = binary_search(n,x,z);

        /*the integration is analytic between tabulated points*/
        int j;
        double value = 0;
        for(j=0;j<i;++j){
                double h = x[j+1]-x[j];

                value += 0.5*(y[j+1]+y[j])*h;
        }
                double h = z - x[i];
                value += 0.5*(lin_interp(n,x,y,z)+y[i])*h;

        return value;
}

double lin_interp_derive(int n, double * x, double * y, double z){ 
        assert( n>1 && z>=x[0] && z<=x[n-1]); 
 
        int i = binary_search(n,x,z); 
 
        double value = (y[i+1]-y[i])/(x[i+1]-x[i]); 
 
        return value;
}

