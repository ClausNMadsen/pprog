#ifndef HAVE_LINSPLINE_H
#define HAVE_LINSPLINE_H
double lin_interp(int n, double * x, double* y, double z);
double lin_interp_integrate(int n, double * x, double* y, double z);
double lin_interp_derive(int n, double * x, double* y, double z);
#endif
