#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<assert.h>

int main(){
	double start = 0;
	double stop = 22;
	int points = 9;
	double step = (stop-start)/(points-1);
	double * x = malloc(points*sizeof(double));
	double * y = malloc(points*sizeof(double));

	fprintf(stdout,"%d\n",points);
	for(int i=0; i<points; i++){
		x[i] = start + i*step;
		y[i] = 1/(1+exp(-2*(x[i]-7)));
		fprintf(stdout,"%g %g\n",x[i],y[i]);
	}

	free(x);
	free(y);
	return 0;
}
