#!/bin/bash
clear

for d in $(find -path './[^.][^pp]*' -type d -prune);
	do
		cd $d
		for d in $(find -path './*' -type d -prune);
			do
				cd $d
				make clean
				make
				cd ..
			done
	
		cd ../
	done
