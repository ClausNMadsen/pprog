#include<stdio.h>
#include<math.h>
#include<gsl/gsl_integration.h>
#include<stdlib.h>
#include<gsl/gsl_multiroots.h>

/*function for part 1*/
double f(double x, void* params){
	return log(x)/sqrt(x);
}

/*norm_int for part 2*/
double norm_int(double x, void *params){
	double alpha = *(double *)params;
	double f = exp(-alpha*pow(x,2));
	return f;
}

/*for part 2*/
double hamiltonian_int(double x, void *params){
	double alpha = *(double *)params;
	double f = (-pow(alpha,2)*pow(x,2)/2 + alpha/2 + pow(x,2)/2)*exp(-alpha*pow(x,2));
	return f;
}

int main(void){

	printf("\nPart 1\n\n");
	printf("Solving definite integral I of f = log(x)/sqrt(x) from 0 to 1\n");
	printf("Analytically, the integral evaluates to I = -4. Gsl gives:\n");

	gsl_integration_workspace* w = gsl_integration_workspace_alloc(100);
	double result, error;

	gsl_function F;
	F.function = f;
	F.params = NULL;

	gsl_integration_qags(&F, 0., 1., 1e-6, 0, 10, w, &result, &error);
	printf("I = %.20g\n",result);


	printf("\nPart 2\n\n");
	printf("Variational method on harmonic oscillator\n");
	printf("natural units, also m = 1 and k = 1 with trial wavefunction\n");
	printf("ψ(x) = exp(-αx^2/2) \n");

	double alpha, result_norm, result_hamiltonian, error_norm, error_hamiltonian;
	FILE* file = fopen("harm.dat","w");

	gsl_function Norm_int;
	Norm_int.function = norm_int;
	Norm_int.params = &alpha;

	gsl_function Hamiltonian_int;
	Hamiltonian_int.function = hamiltonian_int;
	Hamiltonian_int.params = &alpha;

	for(int i = 1; i<500; i++){
		alpha = (double)i/100;
		gsl_integration_qagi(&Norm_int, 0, 1e-8, 10, w, &result_norm, &error_norm);
		gsl_integration_qagi(&Hamiltonian_int, 0, 1e-8, 100, w, &result_hamiltonian, &error_hamiltonian);
		fprintf(file, "%g %g\n", alpha, result_hamiltonian/result_norm);
	}
	printf("For E(a), see harm.svg\n");
	printf("For a=1, the ground state of the HO is exactly reached, with energy E=1/2*hbar*omega = 1/2\n");
	printf("Hence this should be the minimum.\n\n");

	gsl_integration_workspace_free(w);


	return 0;

}
