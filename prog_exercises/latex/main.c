#include<stdio.h>
#include<stdlib.h>

double errfun(double x);

int main(int argc, char** argv){
	if (argc !=4){
		printf("\\Number of arguments provided different from 3. Provide 3 numbers as arguments.\n");
		return 0;
	}

	double a = atof(argv[1]), b = atof(argv[2]), dx = atof(argv[3]);
	if(a > b) {
		printf("First argument must be smaller than second argument. Change provided arguments.\n");
		return 0;
	}

	double res;
	printf("x\tu(x)\n");
	for (double x_tmp = a; x_tmp <b + dx; x_tmp += dx){
		res = errfun(x_tmp);
		printf("%g\t%g\n", x_tmp, res);
	}

	return 0;
}
