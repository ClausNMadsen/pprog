/* Used the example on ODEs from the GSL Manual (https://www.gnu.org/software/gsl/doc/html/ode-initval.html#examples)
 */

#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>
#include<math.h>

int diff(double x, const double u[], double dudx[], void *params){
	dudx[0] = (2/sqrt(M_PI))*exp(-x*x);
	return GSL_SUCCESS;
}

double errfun(double x){
	if(x == 0.0){
		return 0.0; /*starting condition*/
	} else if(x<0.0) { /*odd function, guard negative integration*/
		return -errfun(-x);
	}

	double x0 = 0.0; /*initial condition*/
	double u[1] = {0.0};

	gsl_odeiv2_system sys = {diff, NULL, 1, NULL};

	gsl_odeiv2_driver* d;
	double initial_step_size = 1e-3, abs = 10-6, rel = 0;
	d = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rk8pd, initial_step_size, abs, rel);

	gsl_odeiv2_driver_apply(d, &x0, x, u);

	gsl_odeiv2_driver_free(d);
	return u[0];
}
