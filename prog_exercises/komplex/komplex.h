#ifndef HAVE_KOMPLEX_H /*smart safeguard from multiple includes*/
#define HAVE_KOMPLEX_H

struct komplex {double re; double im;}; /*defines structure komplex*/
typedef struct komplex komplex; 	/*defines the type of structure komplex to be called simply by komplex*/

void komplex_print	(char* s, komplex z);	/*print string s and komplex z*/
void komplex_set 	(komplex* z, double x, double y); 	/*reset komple z*/
komplex komplex_new 	(double x, double y );	/*make new komplex z from doubles x and y*/	
komplex komplex_add	(komplex a,komplex b);	/*add komplex a and b*/
komplex komplex_sub	(komplex a,komplex b);	/*subtract komplex a and b*/

#endif
