#include"komplex.h"
#include<complex.h>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>

#define KOMPLEX(z)  komplex_new(creal(z),imag(Z))
#define RND  (double)rand()/RAND_MAX


int main(){
	komplex a = {RND,RND};
	komplex b = {RND,RND};
	komplex z_set;

	complex test_a = a.re+ I* a.im;	/*testing komplex_print*/
	printf("Testing komplex_print function\n");
	komplex z = {creal(test_a),cimag(test_a)};
	komplex_print("z is \t\t z=",z);
	komplex_print("z should be \t z=",z);
	
	komplex_set(&z_set,b.re,b.im); /*testing komplex_set*/
	printf("Testing komplex_set\n");
	printf("z_set is now \t z=%g+i*%g\n",z_set.re,z_set.im);
	printf("Should be \t z=%g+i*%g\n",b.re,b.im);

	komplex z_new = komplex_new(a.re,b.re); /*testing komplex_new*/
	printf("Testing komplex_new\n");
	printf("z_new is \t z=%g+i*%g\n",z_new.re,z_new.im);
	printf("z_new is \t z=%g+i*%g\n",a.re,b.re);

	komplex z_add = komplex_add(a,b); /*testing komplex_add*/
	printf("Testing komplex_add\n");
	printf("z_add is \t z=%g+i*%g\n",z_add.re,z_add.im);
	printf("Should be \t z=%g+i*%g\n",a.re+b.re,a.im+b.im);

	komplex z_sub = komplex_sub(a,b); /*testing komplex_sub*/
	printf("Testing komplex_sub\n");
	printf("z_sub is \t z=%g+i*%g\n",z_sub.re,z_sub.im);
	printf("Should be \t z=%g+i*%g\n",a.re-b.re,a.im-b.im);
	return 0;
}
