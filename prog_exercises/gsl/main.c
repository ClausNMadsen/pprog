#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_linalg.h>


int main(){
	FILE * mfile = fopen("matrix.txt","r");

	int size1; /*finds the two integers indicating the size of the matrix */
	int size2;
	printf("First trying to read to size integers from matrix.txt.\n");
	if(fscanf(mfile,"%i",&size1) == 1){
		printf("size1 = %d\n",size1);
	} else {
		printf("fscanf to find integer size1 failed\n");
	}

	if(fscanf(mfile,"%i",&size2) == 1){
		printf("size2 = %d\n",size2);
	} else {
		printf("fscanf to find integer size2 failed\n");
	}
	
	gsl_matrix * A = gsl_matrix_alloc(size1,size2); /*allocates matrix mem*/
	gsl_matrix_fscanf(mfile,A);
/* 	for( int i = 0; i<size1; i++) {
		for(int j = 0; j<size2; j++) {
		       printf("A[%d,%d]=%g\n",i,j,gsl_matrix_get(A,i,j));
		}
	}		
*/
	/*uncommenting the above to print matrix A*/


	gsl_vector * b = gsl_vector_alloc(size2);
	gsl_vector_fscanf(mfile,b);
/*	for( int i = 0; i<size2; i++) {
	printf("b[i]=%g\n",gsl_vector_get(b,i));}
*/	
	/*uncomment the above to print vector b*/

	gsl_matrix * QR = gsl_matrix_alloc(size1,size2); /*copy of A for QR-decomp*/
	gsl_matrix_memcpy(QR,A);
	gsl_vector * x = gsl_vector_alloc(size2); /*copy of b for QR-decomp*/
	gsl_vector_memcpy(x,b);
	gsl_vector * tau = gsl_vector_alloc(size2); /*vector tau for QE-decomp, length = MIN(size1,size2)*/
	
	
	gsl_linalg_QR_decomp(QR,tau);
	gsl_linalg_QR_svx(QR,tau,x);

	printf("The result of using QR decomposition to solve the given matrix equation is:\n");
	for( int i = 0; i<size2; i++)
		{printf("x[%d]=%g\n",i,gsl_vector_get(x,i));}
	
	printf("As confirmed by the calculation:\n");
	gsl_vector * x_test = gsl_vector_calloc(size2);

	for( int i = 0; i<size2; i++){
		for( int j = 0; j<size1; j++){
			double pprod = gsl_matrix_get(A,i,j) * gsl_vector_get(x,j);
			double psum = pprod + gsl_vector_get(x_test,i);
			gsl_vector_set(x_test,i,psum);
		}
	}
	for( int i = 0; i<size2; i++){
		printf("A[%d,1]*x[1] + A[%d,2]*x[2] + A[%d,3]*x[3] = %g\n",i+1,i+1,i+1,gsl_vector_get(x_test,i));}
	printf("which should equal:\n");
	for( int i = 0; i<size2; i++){
		printf("b[%d] = %g\n",i,gsl_vector_get(b,i));
	}
	printf("Thus, the found solution to the matrix equation is true.\n");

	gsl_matrix_free(A);
	gsl_vector_free(b);
	gsl_matrix_free(QR);
	gsl_vector_free(tau);
	gsl_vector_free(x);
	gsl_vector_free(x_test);
	return 0;

	}
