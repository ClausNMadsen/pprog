#include<stdio.h>
#include<gsl/gsl_sf_airy.h>

int main(){
#define ACC GSL_PREC_SINGLE
	double x;
	while( scanf("%lg",&x) != EOF ) printf("%g \t %g \t %g\n",x,gsl_sf_airy_Ai(x,ACC),gsl_sf_airy_Bi(x,ACC));
	return 0;
}
