
The finished plot of the Airy functions can be found in plot.svg.
The Matrix equation is solved below.

First trying to read to size integers from matrix.txt.
size1 = 3
size2 = 3
The result of using QR decomposition to solve the given matrix equation is:
x[0]=-1.1379
x[1]=-2.83303
x[2]=0.851459
As confirmed by the calculation:
A[1,1]*x[1] + A[1,2]*x[2] + A[1,3]*x[3] = 6.23
A[2,1]*x[1] + A[2,2]*x[2] + A[2,3]*x[3] = 5.37
A[3,1]*x[1] + A[3,2]*x[2] + A[3,3]*x[3] = 2.29
which should equal:
b[0] = 6.23
b[1] = 5.37
b[2] = 2.29
Thus, the found solution to the matrix equation is true.
