#include"equal.h"
#include<stdio.h>
#include<limits.h>
#include<float.h>

int main(){
	printf("\nPart 1:\nMaximum integer of this machine:\n");
	printf("limits.h gives:\nINT_MAX=\t%i\n",INT_MAX);
	int n;
	for(n=0; (n+1) > 0; n++){
	}
	printf("for loop gives:\nINT_MAX=\t%i\n",n-1);
	
	
	n=0;
	while(n+1 > 0){n++;}
	printf("while loop gives:\nINT_MAX=\t%d\n",n-1);
	
	n= 0;
	do {n++;}
	while(n+1>0);
	printf("do while loop gives:\nINT_MAX=\t%d\n",n-1);


	printf("\nMinimum integer for this machine:\n");
	printf("limits.h gives:\nINT_MIN=\t%i\n",INT_MIN);

	n=0;
	for(n=0; n-1 < 0; n--){};
	printf("for loop gives:\nINT_MIN=\t%i\n",n+1);
	
	n=0;
	while(n-1<0) {n--;}
	printf("while loop gives:\nINT_MIN=\t%i\n",n+1);

	n=0;
	do {n--;}
	while(n-1<0);
	printf("dowhile loop gives:\nINT_MIN=\t%i\n",n+1);


	printf("\nThe machine epsilons of this machine:\n\nFor floats:\n");
	printf("float.h gives:\nFLT_EPSILON=\t%.25f\n",FLT_EPSILON);
	
	float f;
	for(f= 1;f+1!=1;){f/=2;} f*=2;
	printf("for loop gives:\nFLT_EPSILON=\t%.25f\n",f);
		
	
	f=1;
	while(1+f!=1){f/=2;} f*=2;
	printf("while loop gives:\nFLT_EPSILON=\t%.25f\n",f);

	
	f= 1;
	do { f/=2; }
	while(f+1!=1);
	f*=2;
	printf("dowhile loop gives:\nFLT_EPSILON=\t%.25f\n",f);


	printf("\nFor doubles:\nfloat.h gives:\nDBL_EPSILON=\t%.25lg\n",DBL_EPSILON);
	
	double d=1;
	for(d=1; d+1!=1;){d/=2;} d *=2;
	printf("for loop gives:\nDBL_EPSILON=\t%.25lg\n",d);

	d=1;
	while(d+1!=1){d/=2;} d*=2;
	printf("while loop gives:\nDBL_EPSILON=\t%.25lg\n",d);
	
	d= 1;
	do {d/=2;}
	while(d+1!=1);
	d*= 2;
	printf("dowhile loop gives:\nDBL_EPSILON=\t%.25lg\n",d);
	

	printf("\nFor long doubles:\nfloat.h gives:\nLDBL_EPSILON=\t%.25Lg\n",LDBL_EPSILON);
	
	long double ld= 1;
	for(ld=1;ld+1!=1;){ld/=2;} ld*=2;
	printf("for loop gives:\nLDBL_EPSILON=\t%.25Lg\n",ld);
	
	ld= 1;
	while(ld+1!=1){ld/=2;} ld*=2;
	printf("while loop gives:\nLDBL_EPSILON=\t%.25Lg\n",ld);
	
	ld= 1;
	do {ld/=2;}
	while(ld+1!=1);
	ld*= 2;
	printf("do while loop gives:\nLDBL_EPSILON=\t%.25Lg\n",ld);

	printf("\nPart 2:\n");
	int k = 2;
	printf("Using: max=INT_MAX/%i\n",k);
	int max = INT_MAX/k;

	int i;
	float sum_up_float = 0;
	for(i=1;i<max;++i){sum_up_float+=1.0f/i;}
	printf("The sum of floats starting from largest number gives:\nsum_up_float=\t%f\n",sum_up_float);

	float sum_down_float = 0;
	for(i=max;i>0;--i){sum_down_float+=1.0f/i;}
	printf("The sum of floats starting from lowest number gives:\nsum_down_float=\t%f\n",sum_down_float);

	printf("\nGiven that computers have more precision around 0 than other numbers, it would make sense if the very small numbers later on in the sum_up are simply smaller than the machine epsilon at that point, whereas adding the smaller numbers first, closer to 0, grants the machine better precision at the time of the addition of the smaller numbers.\n");

	printf("\nDoes the sum converge as a function of max? It should to some extend as max goes up. For very large max, only a small total contribution of the sum should be contained in very many very small numbers. Testing the hypothesis:\nUsing different divisor integers:\n");

	for(k=10;k<701;k+=80){
		max = INT_MAX/k;
		float sum_up_float = 0;
		for(i=1;i<max;++i){sum_up_float+=1.0f/i;}

		float sum_down_float = 0;
		for(i=max;i>0;--i){sum_down_float+=1.0f/i;}

		printf("\nDivisor = %i:\nsum_up_float=\t%f\nsum_down_float=\t%f\n",k,sum_up_float,sum_down_float);}


	printf("\nGoing back to max=INT_MAX/2, the sums using doubles give:\n");

	k=2;
	max = INT_MAX/k;


	double sum_up_double = 0;
	for(i=1;i<max;++i){sum_up_double+=1.0l/i;}
	printf("The sum of double starting from largest number gives:\nsum_up_double=\t%f\n",sum_up_double);

	double sum_down_double = 0;
	for(i=max;i>0;--i){sum_down_double+=1.0l/i;}
	printf("The sum of double starting from lowest number gives:\nsum_down_double=%f\n",sum_down_double);

	printf("\nThe double class of numbers clearly has enough precision to add all numbers without losing a considerable amount to epsilon limitations.\n");

	// testing the use of equal.h, .c and .o
	printf("\nTesting the equal function from equal.h, defined in equal.c, compiled into equal.o and linked to make this executable\n");
	double a 	= 50.06;
	double b 	= 50.04;
	double tau 	= 0;
	double epsilon  = 0.1;
	printf("\nUsing the parameters:\na\t=%g\nb\t=%g\ntau\t=%g\nepsilon=%g\n",a,b,tau,epsilon);
	equal(a,b,tau,epsilon);
	
	return 0; 
}
