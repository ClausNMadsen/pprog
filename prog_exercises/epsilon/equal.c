#include"equal.h"
#include<stdio.h>
#include<math.h>
#include<stdlib.h>

int equal(double a, double b, double tau, double epsilon){
	if(abs(a-b)<tau){printf("a and b are equal within absolute tolerance tau=%g\n",tau); return 0;}
	else if(abs(a-b)/(abs(a)+abs(b))<epsilon/2){printf("a and b are equal within relative tolerance epsilon=%g\n",epsilon); return 0;}
	else return 0;
}
