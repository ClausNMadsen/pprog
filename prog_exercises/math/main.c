#include<stdio.h>
#include<math.h>
#include<complex.h>
int main(){
	double x_gamma; /*Gamma(5)*/
	double y_gamma;
	x_gamma = 5;
	y_gamma  = tgamma(x_gamma);
	printf("Gamma(5)\t=\t%g\n",y_gamma);

	double x_bessel; /*J1(0.5)*/
	double y_bessel;
	x_bessel = 0.5;
	y_bessel = j1(x_bessel);
	printf("J1(0.5)\t\t=\t%g\n",y_bessel);

	double complex a = csqrt(-2); /*complex sqrt(-2)*/
	printf("sqrt(-2))\t=\t%g + i*%g\n",creal(a),cimag(a));

	double complex etoi = cpow(M_E,I);
	printf("e^(i)\t\t=\t%g + i*%g\n",creal(etoi),cimag(etoi));

	double complex etoipi = cpow(M_E,I*M_PI);
	printf("e^(i*pi)\t=\t%g + i*%g\n",creal(etoipi),cimag(etoipi));

	double complex itoe = cpow(I,M_E);
	printf("i^(e)\t\t=\t%g + i*%g\n",creal(itoe),cimag(itoe));

	float sigdigsf = 0.1111111111111111111111111111;
	double sigdigsd = 0.1111111111111111111111111111;
	long double sigdigsLd = 0.1111111111111111111111111111L;
	printf("When trying to store 0.1111111111111111111111111111,\nfloat, double and long double instead store:\nfloat:\t\t%.25f\ndouble:\t\t%.25g\nlong double:\t%.25Lg\n",sigdigsf,sigdigsd,sigdigsLd);
	return 0;
}
