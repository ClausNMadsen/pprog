#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"
#define RND (double)rand()/RAND_MAX

int main(){

	int n = 5;

	printf("\ntesting nvector_alloc:\n");
	nvector* u = nvector_alloc(n);
	if( u==NULL) printf("nvector memory allocation failed!\n");
	else printf("nvector_alloc passed!\n");

	printf("\ntesting nvector_set and nvector_get:\n");
	double value = RND;
	int i = n/2;
	nvector_set(u,i,value);
	double vi = nvector_get(u,i);
	if( vi == value) printf("test passed\n");
	else printf("something failed in nvector_set or nvector_get\n");

	printf("\ntesting nvector_dot_product\n");
	nvector* v = nvector_alloc(n);
	for(i=0; i<n; i++){v->data[i] = RND; u->data[i] = RND;}
	double sum = nvector_dot_product(u,v);
	double test_sum = 0;
	for(i=0; i < n;i++){test_sum += (v->data[i] + u->data[i]);}
	printf("sum = %g and  test_sum = %g\n",sum,test_sum);
	if(sum == test_sum){printf("nvector_dot_product passed!\n");}
	else printf("nvector_dot_product failed!\n");
	
	nvector_free(u);
	nvector_free(v);
	return 0;	

	}
