#include<stdio.h>
#include<stdlib.h>
#include"nvector.h"

nvector* nvector_alloc(int n){
	nvector* u = malloc(sizeof(nvector));
	(*u).size = n;
	(*u).data = malloc(sizeof(double)*n);
	if( u==NULL ) fprintf(stderr,"error in nvector_alloc\n");
	return u;
}

void nvector_free (nvector* u){
	free((*u).data);
	free(u);
	return;
}

void nvector_set(nvector* u, int i, double value){
	(*u).data[i] = value;
	return;
}

double nvector_get(nvector* u, int i){
	double value = (*u).data[i];
	return value;
}

double nvector_dot_product(nvector* u, nvector* v){
	double sum = 0;
	if((*u).size!=(*v).size) fprintf(stderr,"vectors not same length");
	else for( int i=0; i<(*u).size ; i++) sum+=(*u).data[i]+(*v).data[i];
	return sum;
}
