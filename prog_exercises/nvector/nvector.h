#ifndef HAVE_NVECTOR_H
#define HAVE_NVECTOR_H
typedef struct {int size; double* data;} nvector;

nvector*	nvector_alloc		(int n); /*allocate memory for nvector */
void		nvector_free		(nvector* u); /*free memory of nvector */
void 		nvector_set		(nvector* u, int i, double value); /*set i'th element=value*/
double 		nvector_get 		(nvector* u, int i); /*get value of i'th element*/
double 		nvector_dot_product	(nvector* u, nvector* v); /*calculate dot product of u and v*/


#endif
