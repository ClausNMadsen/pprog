#ifndef HAVE_MY_ARCTAN_H
#define HAVE_MY_ARCTAN_H
int arctan_ode(double x, const double y[], double dydt[], void* params);
double my_arctan(double x);
#endif
