#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"my_arctan.h"

int main(int argc, char** argv){
	if(argc != 4){fprintf(stderr,"Give three arguments, a, b and dx.\n");return 1;}	
	double a = atof(argv[1]);
	double b = atof(argv[2]);
	double dx = atof(argv[3]);
	if(dx == 0.0){fprintf(stderr,"Give a non-zero step size.\n");return 1;}
	printf("Exercise 22: Arctangent from the ODE:\natan'(x)=1/(x^2+1)\natan(0)=0\n");
	
	FILE* data = fopen("data.txt", "w");
	for(double x=a; x<=b; x += dx){
		fprintf(data," %g %g %g\n",x,my_arctan(x),atan(x));
	}
	fclose(data);
	printf("A plot with comparisons of my arctangent function from the ODE and atan(x) included in math.h is made and used in report.pdf. atan2 was not used as it was not deemed necessary for this exercise.\n");
	return 0;
}
