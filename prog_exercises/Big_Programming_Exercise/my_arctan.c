#include"my_arctan.h"
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int arctan_ode(double x, const double y[], double dydt[], void* params){
	dydt[0]= 1/(x*x+1.0);
return GSL_SUCCESS;
}

double my_arctan(double x){
	if (x<0){return -my_arctan(-x);};
	double hstart = 1e-3,abs=1e-9,eps=1e-9;
	gsl_odeiv2_system sys;
	sys.function = arctan_ode;
	sys.jacobian = NULL;
	sys.dimension = 1;
	sys.params = NULL;
	gsl_odeiv2_driver* driver;
	driver = gsl_odeiv2_driver_alloc_y_new(
			&sys,
			gsl_odeiv2_step_rkf45,
			hstart,
			abs,
			eps);

	double x0=0.0;
	double y[] = {0};
	gsl_odeiv2_driver_apply(driver, &x0, x, y);
	gsl_odeiv2_driver_free(driver);
	return y[0];
}
