#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_multiroots.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

/*basic differential equation*/
int SE(double x, double y[], double dy[], void * params){
	double E = *(double*)params;
	dy[0] = y[1];
	dy[1] = 2*(-1/x-E) *y[0];
	return GSL_SUCCESS;
}

/*runs difeq driver with rkf45 step*/
double Fe(double E, double r){
	
	double rmin = 1e-3;
	if(r<rmin) return r-r*r;

	gsl_odeiv2_system system;
	system.function = SE;
	system.jacobian = NULL;
	system.dimension = 2;
	system.params = (void*)&E;

	double t = rmin;
	double y[] = {t-t*t, 1-2*t};
	gsl_odeiv2_driver * driver = gsl_odeiv2_driver_alloc_y_new(&system, gsl_odeiv2_step_rkf45, 1e-3, 1e-5, 1e-6);
	gsl_odeiv2_driver_apply(driver, &t, r, y);


	gsl_odeiv2_driver_free(driver);
	return y[0];
}
