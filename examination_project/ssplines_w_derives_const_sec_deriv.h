#ifndef HAVE_SSPLINES_W_DERIVES_CONST_SEC_DERIVES_H
#define HAVE_SSPLINES_W_DERIVES_CONST_SEC_DERIVES_H
typedef struct {int n; double *x, *y, *b, *c, *d, *e;} sspline_w_derives_const_sec_deriv;
sspline_w_derives_const_sec_deriv* sspline_w_derives_const_sec_deriv_alloc(int n, double* x, double* y, double* y_prime);
double sspline_w_derives_const_sec_deriv_eval(sspline_w_derives_const_sec_deriv* s, double z);
void sspline_w_derives_const_sec_deriv_free(sspline_w_derives_const_sec_deriv* s);
double sspline_w_derives_const_sec_deriv_derive(sspline_w_derives_const_sec_deriv* s, double z);
#endif
