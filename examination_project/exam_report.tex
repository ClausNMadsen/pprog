\documentclass[english]{article}
\usepackage[T1]{fontenc}
\usepackage[latin9]{inputenc}
\usepackage{textcomp}
\usepackage{amsmath}
\usepackage{babel}
\usepackage{graphicx}
\begin{document}

\title{Report on examination project, exercise 0:\\\large{\emph{Cubic sub-spline for data with derivatives}}}
\date{\today}
\author{Claus N. Madsen, studienummer 201505292}
\maketitle

\section{The problem}
Since my student number ends in 92 and there are 23 exercises, I got exercise 92\%23=0. The problem is the following. Given a set of data consisting of points and the value of a function and its derivative at these points, $\{x_i,y_i,y'_i\}_{i=1,...,n}$, construct a cubic spline making use of the tabulated derivative. More concisely, given a data-set $\{x_i,y_i,y'_i\}_{i=1,...,n}$, build a cubic sub-spline,
\begin{equation}
S_i(x)=y_i + b_i(x-x_i) + c_i(x-x_i)^2 + d_i(x-x_i)^3,
\end{equation}
where the coefficients $b_i$, $c_i$ and $d_i$ are determined by the three conditions,
\begin{align}
&S_i(x_{i+1}) = y_{y+1},\\
&S'_i(x_i) = y'_i,\\
&S'_i(x_{i+1}) = y'_{i+1}.
\end{align}
Additionally, two extra sub-assignments were given. These were
\begin{enumerate}
\item Implement the derivative and integral of the spline, and
\item Make the second derivative of the spline continuous by increasing the order of the spline to four, for example, in the form
\begin{equation}
S_i(x) = y_i + b_i(x-x_i) + c_i(x-x_i)^2 + d_i(x-x_i)^3 + e_i(x-x_i)^2(x-x_{i+1})^2.
\end{equation}
\end{enumerate}
Both the main problem and the two extra problems have been completed.

\section{A solution}
A hint was given in the problem description to look at the Akima-spline section of Dmitri's lecture notes in the e-book Yet Another Introduction to Numerical Methods for inspiration. In the notes, a derivation for recursion relations in a generea cubic spline are outlined. They are as follows. Given a general cubic spline
\begin{equation}
\mathcal{A}(x)\big\rvert_{x\in\left[x_i,x_{i+1}\right]} =  y_i + b_i(x-x_i) + c_i(x-x_i)^2 + d_i(x-x_i)^3 = \mathcal{A}_i(x).\label{spline_definition}
\end{equation}
Depending on which conditions are imposed on the spline, different recurrence relations are obtainable. Demanding only continuity of the spline and of the first derivative of the spline, 
\begin{equation}
A_{i}\left(x_{i}\right)=y_{i}, A_{i}^{\prime}\left(x_{i}\right)=\mathcal{A}_{i}^{\prime}, A_{i}\left(x_{i+1}\right)=y_{i+1}, A_{i}^{\prime}\left(x_{i+1}\right)=\mathcal{A}_{i+1}^{\prime},\label{conditions}
\end{equation}
one can insert \ref{spline_definition} into \ref{conditions} and solve for the coefficients to find the relations
\begin{equation}
a_{i}=y_{i}, b_{i}=\mathcal{A}_{i}^{\prime}, c_{i}=\frac{3 p_{i}-2 \mathcal{A}_{i}^{\prime}-\mathcal{A}_{i+1}^{\prime}}{\Delta x_{i}}, d_{i}=\frac{\mathcal{A}_{i}^{\prime}+\mathcal{A}_{i+1}^{\prime}-2 p_{i}}{\left(\Delta x_{i}\right)^{2}}.\label{relations}
\end{equation}
Given the problem description, now we simply set $\mathcal{A}'_i(x_i)=y_i'$ and $\mathcal{A}'_i(x_{i+1})=y_{i+1}'$. Thus, once this step is reached, the mathematics of the problem are solved and the remaining task is implementation.
\section{Implementation}
All implemented functions related to the spline use the typedef'd structure found in \texttt{csplines\_w\_derives.h} called \texttt{cspline\_w\_derives}. The implementation of the cubic sub-spline is found in the file 
\begin{quote}
\texttt{csplines\_w\_derives.c}.
\end{quote}
Herein lies the three main functions,
\begin{quote}
\texttt{cspline\_w\_derives* cspline\_w\_derives\_alloc(int n, double* x, double* y, double* y\_prime)}\\
\texttt{double cspline\_w\_derives\_eval(cspline\_w\_derives* s, double z)}\\
\texttt{void cspline\_w\_derives\_free(cspline\_w\_derives* s)}
\end{quote}
The most complicated of these three is the \texttt{alloc} function. This function takes the number of tabulated data points and the points themselves with the value of the function and its derivative in each point and constructs the spline from them. Some definitions are used to make the notation in the calculations less dense. These are $h_i = x_{i+1}-x_i$ and $p_i=\frac{y_{i+1}-y_i}{h_i}$. Other than this nothing interesting is going on. The recurrence relations from \ref{relations} are simply used to get the coefficients which are then stored in the \texttt{cspline\_w\_derives} struct s. What takes up the most space in this function is simple memory allocation for the spline and all its parameters.\\
The \texttt{eval} function takes the \texttt{cspline\_w\_derives} struct, a double, $z$, and returns the value of the spline in that point. If $z$ is a point in the data range, a binary search is performed to find which interval $[x_i,x_{i+1}]$´ it is found in. Then the coefficients of that interval are used for the calculation. If the point is not within the range of provided $x_i$'s, an error message is printed and the procedure is stopped.\\
Lastly, the \texttt{free} function first frees each \texttt{malloc}'d member of the struct and then the struct itself.\\
To showcase how this sub spline has less squiggles than a regular cubic spline, a comparison between the analytical function, my sub spline from points, and a cubic spline from the same points. I implemented the cubic spline in the \texttt{interpolation} exercise and showed that it matches the output of the GSL implementation. This comparison is found in figure~\ref{comparison1}.

\begin{figure}
\includegraphics[width=1\linewidth]{plot1.pdf}
\caption{A comparison of this sub spline to a regular cubic spline and the analytical function.}
\label{comparison1}
\end{figure}

\section{Extra}
The first extra task was to implement the derivative and integral of the spline function. This has been done in the \texttt{csplines\_w\_derives.c} library in the form of the two functions
\begin{quote}
\texttt{double cspline\_w\_derives\_derive(cspline\_w\_derives* s, double z)}\\
\texttt{double cspline\_w\_derives\_integrate(cspline\_w\_derives* s, double z)}.
\end{quote}
The \texttt{derive} function takes the sub spline and the point in which to evaluate the derivative of the sub spline. First it establishes that z is inside the valid region, then it uses binary search to establish which interval z is found in. Then it simply returns the derivative of the spline with the correct coefficients. A comparison of the derivative of the sub spline with the analytical expression and the derivative of the cubic spline is found in figure~\ref{deriv_comparison}. While the sub spline derivative clearly squiggles less, it is also decidedly less smooth, with visible breaks of curvature.
\begin{figure}
\includegraphics[width=\linewidth]{plot_deriv_comparison.pdf}
\caption{A comparison between the derivative of my sub spline, the analytical expression and of the cubic spline.}
\label{deriv_comparison}
\end{figure}
The \texttt{integrate} function likewise takes the sub spline and the point to which it should integrate. However, since it always integrates from the leftmost point of the spline, no binary search is needed to find which interval z lies in. The function simply integrates from the left until it hits the right interval. A comparison between the sub spline integral, the analytical result and the cubic spline integral is shown in figure~\ref{antideriv_comparison}. As expected, all three integrals are very close. As Dmitri taught us, integration is good and differentiation is bad. Numerical integration produces less noiseful results and numerical differentiation enhances noise.
\begin{figure}
\includegraphics[width=\linewidth]{plot_antiderivativ_comparison.pdf}
\caption{A comparison between the integral of the cubic sub spline, the analytical result and the integral of the cubic spline.}
\label{antideriv_comparison}
\end{figure}\\\\
The second extra problem was to increase the order of the sub spline and make the second derivative of the sub spline continuous. The higher order form of the sub spline was chosen as in the description to be
\begin{equation}
S_{i}(x)=y_{i}+b_{i}\left(x-x_{i}\right)+c_{i}\left(x-x_{i}\right)^{2}+d_{i}\left(x-x_{i}\right)^{3}+e_{i}\left(x-x_{i}\right)^{2}\left(x-x_{i+1}\right)^{2}.\label{highorder}
\end{equation}
The beauty of this higher order form is that all coefficients other than $e_i$ are found as before, since the term with this coefficient drops out of all the conditions. Demanding a continuous second derivative amounts to demanding
\begin{equation}
S''_i(x_{i+1}) = S''_{i+1}(x_{i+1})\label{secondcondition}.
\end{equation} 
Inserting \ref{highorder} into \ref{secondcondition} gives the relation
\begin{equation}
c_i + 3d_i(x_{i+1}-x_i)+e_i(x_{i+1}-x_i)^2=e_{i+1}(x_{i+2}-x_{i+1})^2 + c_{i+1}.
\end{equation}
With notation from earlier, this gives the recurrence relation
\begin{equation}
e_i=e_{i+1}\left(\frac{h_{i+1}}{h_i}\right)^2+\frac{c_{i+1}-c_i}{h_i^2}-\frac{3d_i}{h_i}.
\end{equation}
As in the cubic spline from the Numerical Methods exercise, two more conditions are used to find the $e_i$ coefficients. First a recurrence run is made with $e_0=0$, then a run backwards with $e_{n-1}=0$ and the two sets of coefficients are averaged together. The implementation of this spline is found in
\begin{quote}
\texttt{sspline\_w\_derives\_const\_sec\_deriv.c}.
\end{quote}
\texttt{alloc}, \texttt{eval} and \texttt{free} functions are implemented as for the sub spline with the small changes needed for this spline. This sub spline is compared against the two other in figure~\ref{highcomparison}. As I was playing around with the implementation it quickly became obvious that the higher order induces more squiggles again. These get smaller with more data points, but they are always an eyesore. They can be avoided somewhat with better guesses at $e_0$ and $e_{n-1}$, but as it stands, each boundary condition makes the part on other side of the peak of the derivative of the logistic function squiggle. As is, the cubic sub-spline seems the most well-behaved spline to apply in a general case without more intimate knowledge of the data or function at hand. Just more data. The quartic sub spline is a lot prettier/fits a lot better for a less steep logistic function, but I decidedly included this to show the problems of squiggles returning. If the function is already easy to handle, then the cubic spline alone should be enough. As with most splines, the quartic sub-spline improves with better knowledge of the function. More points make it converge better towards the actual function. 
\begin{figure}
\includegraphics[width=\linewidth]{plot_higher_comparison.pdf}
\caption{A comparison of the new higher order sub spline to the cubic sub spline, the analytical result and the cubic spline.}
\label{highcomparison}
\end{figure}
A derivative of the quartic sub spline was also implemented. An integral was deemed unnecessary due to the high precision of all the splines. This derivative is compared to that of the other splines in figure~\ref{deriv}.
\begin{figure}
\includegraphics[width=\linewidth]{plot_high_deriv.pdf}
\caption{A comparison of the different derivatives. The squiggle behaviour of the quartic sub spline is clear.}
\label{deriv}
\end{figure}
\end{document}