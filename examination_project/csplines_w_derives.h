#ifndef HAVE_CSPLINES_W_DERIVES_H_W_DERIVES_H
#define HAVE_CSPLINES_W_DERIVES_H_W_DERIVES_H
typedef struct {int n; double *x, *y, *b, *c, *d;} cspline_w_derives;
cspline_w_derives* cspline_w_derives_alloc(int n, double* x, double* y, double* y_prime);
double cspline_w_derives_eval(cspline_w_derives* s, double z);
void cspline_w_derives_free(cspline_w_derives* s);
double cspline_w_derives_derive(cspline_w_derives* s, double z);
double cspline_w_derives_integrate(cspline_w_derives* s, double z);
#endif

