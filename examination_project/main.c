#include<stdio.h>
#include<math.h>
#include"csplines_w_derives.h"
#include"cspline.h"
#include"ssplines_w_derives_const_sec_deriv.h"

/*My chosen test function*/
double test_function(double x){
	return 1.0/(1.0+exp(-5*(x-5)));
}
 

/*and its derivative*/
double test_function_derivative(double x){
	return 5*exp(-5*(x-5))/pow(1.0+exp(-5*(x-5)),2);
}

/*and its anti-derivative from 0*/
double test_function_antiderivative(double x){
	return 1.0/5.0*(log(exp(5*x)+exp(25))-log(1+exp(25)));
}

int main(void){
	int points = 14;
	double start = 0.0, stop = 10.0;
	int plotpoints = 1000;
	double step = (stop-start)/(points-1.0);
	double x[points], y[points], y_prime[points];
	for(int i=0; i<points; ++i){
		x[i] = start + i*step;
		y[i] = test_function(x[i]);
		y_prime[i] = test_function_derivative(x[i]);
	}
	cspline_w_derives* cspline_wd = cspline_w_derives_alloc(points,x,y,y_prime);
	cspline* cspline = cspline_alloc(points,x,y);
	sspline_w_derives_const_sec_deriv* sspline =  sspline_w_derives_const_sec_deriv_alloc(points,x,y,y_prime);

	FILE* data = fopen("data.txt","w");
	for(int i=0; i<points; ++i){
		fprintf(data,"%g %g\n",x[i],y[i]);
	}
	fprintf(data,"\n\n\n");
	double x_plot;
	for(int i=0; i<plotpoints;++i){
		x_plot = (double)i/plotpoints*(stop-start);
		fprintf(data,"%g %g %g %g\n",x_plot, cspline_w_derives_eval(cspline_wd,x_plot), test_function(x_plot),cspline_eval(cspline,x_plot));
	}
	fclose(data);

	FILE* data_derives = fopen("data_deriv_comparison.txt","w");
	for(int i=0; i<plotpoints; ++i){
		x_plot = (double)i/plotpoints*(stop-start);
		fprintf(data_derives,"%g %g %g %g\n",x_plot, cspline_w_derives_derive(cspline_wd,x_plot), test_function_derivative(x_plot),cspline_derive(cspline,x_plot));
	}
	fclose(data_derives);

	FILE* data_antiderives = fopen("data_antiderivativ_comparison.txt","w");
	for(int i=0; i<plotpoints; ++i){
		x_plot = (double)i/plotpoints*(stop-start);
                fprintf(data_antiderives,"%g %g %g %g\n",x_plot, cspline_w_derives_integrate(cspline_wd,x_plot), test_function_antiderivative(x_plot),cspline_integrate(cspline,x_plot));
        }
	
	FILE* data_highcomparison = fopen("data_higher_comparison.txt","w");
	for(int i=0; i<plotpoints; ++i){
		x_plot = (double)i/plotpoints*(stop-start);
		fprintf(data_highcomparison,"%g %g %g %g %g\n",x_plot, cspline_w_derives_eval(cspline_wd,x_plot), test_function(x_plot),cspline_eval(cspline,x_plot),sspline_w_derives_const_sec_deriv_eval(sspline,x_plot));
	}
	fclose(data_highcomparison);

	FILE* data_highderiv = fopen("data_higher_deriv.txt","w");
	for(int i=0; i<plotpoints; ++i){
                x_plot = (double)i/plotpoints*(stop-start);
                fprintf(data_highderiv,"%g %g %g %g %g\n",x_plot, cspline_w_derives_derive(cspline_wd,x_plot), test_function_derivative(x_plot),cspline_derive(cspline,x_plot),sspline_w_derives_const_sec_deriv_derive(sspline,x_plot));
        }
        fclose(data_highderiv);

			

	cspline_w_derives_free(cspline_wd);
	cspline_free(cspline);
	sspline_w_derives_const_sec_deriv_free(sspline);

	printf("Multiple plots comparing this implementation of both a cubic sub-spline and a quartic sub-spline have been produced and included in the exam_report.pdf.\n");
	return 0;
}
