#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include"ssplines_w_derives_const_sec_deriv.h"

sspline_w_derives_const_sec_deriv* sspline_w_derives_const_sec_deriv_alloc(int n, double* x, double* y, double* y_prime){
        if(n<3){fprintf(stderr,"Too few points passed to sspline_w_derives_const_sec_deriv_alloc to construct spline.\n");return NULL;}
        double p[n-1], h[n-1];
        for(int i=0; i<n-1; ++i){h[i] = x[i+1]-x[i];if(h<=0){fprintf(stderr,"Negative or 0 dx stepsize passed to cspline_w_derives_alloc.\n");return NULL;}}
        for(int i=0; i<n-1; ++i){p[i] = (y[i+1]-y[i])/h[i];}
        sspline_w_derives_const_sec_deriv* s = (sspline_w_derives_const_sec_deriv*)malloc(sizeof(sspline_w_derives_const_sec_deriv));
        s->x = (double*)malloc(n*sizeof(double));
        s->y = (double*)malloc(n*sizeof(double));
        s->b = (double*)malloc(n*sizeof(double));
        s->c = (double*)malloc((n-1)*sizeof(double));
        s->d = (double*)malloc((n-1)*sizeof(double));
        s->e = (double*)malloc((n-1)*sizeof(double));
        s->n = n; for(int i=0; i<n; ++i){s->x[i]=x[i];s->y[i]=y[i];s->b[i]=y_prime[i];}
        for(int i=0; i<n-1; ++i){
                s->c[i] =(3*p[i]-2*s->b[i]-s->b[i+1])/h[i];
                s->d[i]=(s->b[i+1]+s->b[i]-2*p[i])/h[i]/h[i];

        }
	/*naive guess is e[0] =0 or e[n-1]=0. As Dmi did in cubic spline, make both guesses and average*/
	double e_forward_guess[n-1], e_backward_guess[n-1]; 
	e_forward_guess[0] = 0.0;

	for(int i=0; i<s->n-2; ++i){
		e_forward_guess[i+1] = (3*s->d[i]*h[i] + e_forward_guess[i]*h[i]*h[i] + s->c[i]-s->c[i+1])/(h[i+1]*h[i+1]);
	}
	
	e_backward_guess[n-2] = 0.0;	
	for(int i=s->n-3; i>=0; --i){
		e_backward_guess[i] = e_backward_guess[i+1]*h[i+1]*h[i+1]/h[i]/h[i] + (s->c[i+1]-s->c[i])/h[i]/h[i] - 3.0*s->d[i]/h[i]; 
	}
	for(int i=0; i<n-1; i++){
		s->e[i] = e_backward_guess[i]+e_forward_guess[i]/2.0;
	}
	return s;
}


double sspline_w_derives_const_sec_deriv_eval(sspline_w_derives_const_sec_deriv* s, double z){
        if(z<s->x[0] || z>s->x[s->n-1]){fprintf(stderr,"Point passed to sspline_w_derives_const_sec_deriv_eval is outside the x-range of the data and spline interpolation.\n");return 1;}
        int i = 0, j=s->n-1;
        while(j-i>1){int m = (i+j)/2; if(z>s->x[m]) i=m; else j=m;}
        double h= z - s->x[i];
        return s->y[i]+h*(s->b[i]+h*(s->c[i]+h*s->d[i])) + -2.0*s->e[i]*(z-s->x[i])*(z-s->x[i])*(z-s->x[i+1])*(z-s->x[i+1]);
}

double sspline_w_derives_const_sec_deriv_derive(sspline_w_derives_const_sec_deriv* s, double z){
        if(z<s->x[0] || z>s->x[s->n-1]){fprintf(stderr,"Point passed to sspline_w_derives_const_sec_deriv_derive is outside the x-range of the data and spline interpolation.\n");return 1;}
        int i = 0, j=s->n-1;
        while(j-i>1){int m = (i+j)/2; if(z>s->x[m]) i=m; else j=m;}
        double h= z - s->x[i];
	return s->b[i] + 2.0*s->c[i]*h + 3.0*s->d[i]*h*h - 2.0*s->e[i]*h*(z-s->x[i+1])*(s->x[i]+s->x[i+1]-2*z);
}


void sspline_w_derives_const_sec_deriv_free(sspline_w_derives_const_sec_deriv* s){
	free(s->x);free(s->y);free(s->b);free(s->c);free(s->d);free(s->e);free(s);return;}
